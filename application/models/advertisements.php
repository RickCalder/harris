<?php
	class Advertisements extends MY_Model {
		protected $primaryKey = "adId";
		public $table = "advertisements";

		protected $adId;
		protected $adTitle;
		protected $adBody;
		protected $adTypeId;
		protected $adPublic;
		protected $createdDateTime;
		protected $adActive;
		protected $createdBy;
		protected $adExpires;
		protected $display;

	function getAdvertisements($adId=0,$userId = 0)
	{
		if($adId !=0)
		{
			$this->db->where('adId',$adId);
		}
		if($userId != 0)
		{
			$this->db->where('createdBy',$userId);
		}
		$this->db->where('adActive',1);
		$this->db->order_by('adId','DESC');
		$this->db->join('ad_types','ad_types.adTypeId=advertisements.adTypeId');
		$data = array();
		$data = $this->db->get('advertisements');
		return $data->result_array();
	}

	function displayAds()
	{
		$this->db->where('adActive',1);
		$this->db->where('display',1);
		$this->db->order_by('adId','DESC');
		$this->db->join('ad_types','ad_types.adTypeId=advertisements.adTypeId');
		$this->db->where('adExpires >=',date('Y-m-d h:i:s'));
		$this->db->limit(2);
		$data = array();
		$data = $this->db->get('advertisements');
		return $data->result_array();
	}

	function getAdsForDisplay($public = 0)
	{
		if($public == 1)
		{
			$this->db->where('adPublic',1);
		}
		$this->db->where('adActive',1);
		$this->db->where('display',1);
		$this->db->order_by('adId','DESC');
		$this->db->join('ad_types','ad_types.adTypeId=advertisements.adTypeId');
		$this->db->where('adExpires >=',date('Y-m-d h:i:s'));
		$data = array();
		$data = $this->db->get('advertisements');
		return $data->result_array();
	}
	}
