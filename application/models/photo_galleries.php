<?php

	class Photo_galleries extends MY_Model {
		protected $primaryKey = "galleryId";
		public $table = "photo_galleries";		
	
		protected $galleryId;	
		protected $galleryName;		
		protected $galleryFolderName;
		protected $galleryActive;
		protected $creationDateTime;

		function getAllGalleries($admin = 1)
		{
			if($admin == 0)
			{
				$this->db->where('galleryActive',1);
			} else{

				$this->db->where('galleryActive',1);
				$this->db->or_where('galleryActive',0);
			}
			$this->db->where('imageActive',1);
			$this->db->join('gallery_images','gallery_images.galleryId = photo_galleries.galleryId');
			$this->db->select('
				photo_galleries.galleryId,
				galleryName, 
				COUNT(galleryImageId) as imageCount, 
				creationDateTime as date, 
				photo_galleries.galleryFolderName,
				galleryActive
			');
			$this->db->group_by('galleryId');
			$this->db->order_by('creationDateTime','DESC');
			$data = $this->db->get('photo_galleries');
			if($data->num_rows() > 0)
			{
				return $data->result_array();
			} else {
				return $data=array();
			}
		}
		
		function getGallery($galleryId, $admin = 0)
		{
			$this->db->where('photo_galleries.galleryId',$galleryId);
			$this->db->join('gallery_images','gallery_images.galleryId = photo_galleries.galleryId');
			if($admin != 1)
			{
				$this->db->where('imageActive',1);
				$this->db->where('galleryActive',1);
			}
			$this->db->where('imageActive <>',2);
			$query = $this->db->get('photo_galleries');
			if($query->num_rows() > 0)
			{
				return $query->result_array();
			}
		}
		
	}
	