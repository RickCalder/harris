<?php
	class Announcements extends MY_Model {
		protected $primaryKey = "announcementId";
		public $table = "announcements";

		protected $announcementId;
		protected $announcementTitle;
		protected $announcementBody;
		protected $active;
		protected $displayStart;
		protected $displayUntil;
		protected $createdBy;

	function getAnnouncements($announcementId=0)
	{
		if($announcementId !=0)
		{
			$this->db->where('announcementId',$announcementId);
		}
		$this->db->where('active',1);
		$this->db->order_by('announcementId','DESC');
		$data = array();
		$data = $this->db->get('announcements');
		return $data->result_array();
	}

	function displayAnnouncements()
	{
		$this->db->where('active',1);
		$this->db->where('displayStart <=',date('Y-m-d h:i:s'));
		$this->db->where('displayUntil >=',date('Y-m-d h:i:s'));
		$this->db->order_by('announcementId','DESC');
		$this->db->limit(2);
		$data=array();
		$data= $this->db->get('announcements');
		return $data->result_array();
	}
	function getAnnounceForMainPage()
	{
		$this->db->where('active',1);
		$this->db->where('displayStart <=',date('Y-m-d h:i:s'));
		$this->db->where('displayUntil >=',date('Y-m-d h:i:s'));
		$this->db->order_by('announcementId','DESC');
		$data=array();
		$data= $this->db->get('announcements');
		return $data->result_array();
	}
	}
