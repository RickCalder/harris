<?php
	class Newsletters extends MY_Model {
		protected $primaryKey = "newsletterId";
		public $table = "newsletters";		
	
		protected $newsletterId;	
		protected $newsletterHeading;		
		protected $newsletterBody;
		protected $active;	
		protected $creationDateTime;
		protected $updatedDateTime;			
		protected $type;
		
			
		function listNewslettersDropdown() {
			$this->db->limit(4,0);
			$this->db->order_by('creationDateTime','DESC');
			$this->db->where('active',1);
			$this->db->where('type', 'newsletter');
			$query = $this->db->get('newsletters');
			$data=array();
			$data[0]="Select a Newsletter";
				if ($query->num_rows >= 1)
				{
					foreach($query->result_array() as $row)
					{
						$data[$row['newsletterId']]=$row['newsletterHeading'];
					}
					$data[9999999]="View Archived Newsletters";
					return $data;
				}
		}	
		
		function getNewsletters($newsletterId=0,$active=1) {
			if(!$newsletterId==0)
			{
				$this->db->where('newsletterId',$newsletterId);
			}
			if($active==1)
			{
				$this->db->where('active ',1);
			} else {
				$this->db->where('active !=',2);
			}
			$this->db->where('type', 'newsletter');
			$this->db->order_by('creationDateTime','DESC');
			$query = $this->db->get('newsletters');
			return $query->result_array();
		}

		function getDocuments($newsletterId=0,$active=1) {
			if(!$newsletterId==0)
			{
				$this->db->where('newsletterId',$newsletterId);
			}
			if($active==1)
			{
				$this->db->where('active ',1);
			} else {
				$this->db->where('active !=',2);
			}
			$this->db->order_by('creationDateTime','DESC');
			$query = $this->db->get('newsletters');
			return $query->result_array();
		}

			
		function listPeriodicDropdown() {
			$this->db->limit(4,0);
			$this->db->order_by('creationDateTime','DESC');
			$this->db->where('active',1);
			$this->db->where('type', 'pi');
			$query = $this->db->get('newsletters');
			$data=array();
			$data[0]="Select a PIC";
				if ($query->num_rows >= 1)
				{
					foreach($query->result_array() as $row)
					{
						$data[$row['newsletterId']]=$row['newsletterHeading'];
					}
					$data[9999999]="View Archived PICs";
					return $data;
				} else {
					$data[9999999]="View Archived PICs";
					return $data;
					
				}
		}	
		
		function getPeriodic($newsletterId=0,$active=1) {
			if(!$newsletterId==0)
			{
				$this->db->where('newsletterId',$newsletterId);
			}
			if($active==1)
			{
				$this->db->where('active ',1);
			} else {
				$this->db->where('active !=',2);
			}
			$this->db->where('type', 'pi');
			$this->db->order_by('creationDateTime','DESC');
			$query = $this->db->get('newsletters');
			return $query->result_array();
		}
	}
	