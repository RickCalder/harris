<?php
	class Ad_types extends MY_Model {
		protected $primaryKey = "adTypeId";
		public $table = "ad_types";

		protected $adTypeId;
		protected $adType;

		public function getAdTypes()
		{
			$query = $this->db->get('ad_types');
			$data=array();
			foreach($query->result_array() as $item)
			{
				$data[$item['adTypeId']]=$item['adType'];
			}
			return $data;
		}
	}

