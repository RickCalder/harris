<div class="content content-full">
<div class="rounded main-box top content-full">
	<div class="right" style="width:400px;">
	<h3>Instructions</h3>

	<p class="small"><strong>Public - Yes means everyone can see the ad, No means only Owners can see the ad.</strong></p>
	<hr />
	<p class="small">The body of the ad is limited to 255 characters, that does include any HTML tags (see below).</p>
	<hr />
	<p class="small">*Please note - Classified Ads must be approved by an administrator before they are publicly viewable.</p>
	<hr />
	<p class="small">Advertisements will run for 2 weeks following the date they are created. To extend an advertisement click edit and resubmit the ad.</p>
	<hr />
	<p class="small">
		The body of the advertisements can take some basic HTML to help you format your message and highlight important parts of the message. <br />You can use the following commands:<br /><br />
		&lt;strong&gt;<strong>Bold text</strong>&lt;/strong&gt;<br />
		<em>&lt;em&gt;Italic text&lt;/em&gt;</em><br />
		<u>&lt;u&gt;Underlined text&lt;/u&gt;</u><br />
		Line break&lt;br /&gt;<br />
	</p>
	</div>
	<h3>Advertisements</h3>
	<div class="form_errors" id="error"><?=validation_errors();?></div>
	<?php
		$attributes=array('name'=>'advertisements','class'=>'formClass');
		echo form_open(base_url().'owners/owner_advertisements',$attributes);
		echo '<input type="hidden" name="adActive" value="1" />';
		echo '<input type="hidden" name="createdBy" value="'.$this->session->userdata('userId').'" />';
		echo '<p style="margin-bottom:5px;">';
		echo form_label('Title: ','title');
		$formData = array(
			'name'=>'adTitle',
			'id'=>'adTitle',
			'value'=>set_value('adTitle')
		);
		echo form_input($formData);
		echo '</p><p style="margin-bottom:5px;">';

		echo '<p style="margin-bottom:5px;">';
		echo form_label('Ad Type: ','adType');
		echo form_dropdown('adType',$adTypes,set_value('adType'),'id="adType"');
		echo '</p><p style="margin-bottom:5px;">';

		echo '<p style="margin-bottom:5px;">';
		echo form_label('Public: ','adPublic');
		$array = array(0=>'No',1=>'Yes');
		echo form_dropdown('adPublic',$array,set_value('adPublic'),'id="adPublic"');
		echo '</p><p style="margin-bottom:5px;">';

		echo '</p><p class="textareabox" style="margin-bottom:5px;">';
		echo form_label('Body: ','body');
		$formData = array(
			'name'=>'adBody',
			'id'=>'adBody',
			'style'=>'height:125px;',
			'value'=>set_value('adBody')
		);
		echo form_textarea($formData);
		echo '</p><p style="width:150px;margin-left:250px;">';
		$formData = array(
			'name'=>'submit',
			'id'=>'submit',
			'class'=>'button',
			'value'=>'Submit'
		);
		echo form_submit($formData);
		echo form_close();
	?>
	<h3>Optional - Insert links in body of ad</h3>
	<p class="small">
		<label>Insert Email Link</label><br />
		<input type="text" id="emailLink" /><span class="xsmall"> Ex: john.doe@google.com</span>
		<input type="submit" value="Insert"  id="createEmail"/>
	</p>
	<p class="small">
		<label>Insert Web Link</label><br />
		<label>Link</label><input type="text" id="webLink" /><span class="xsmall"> Ex: www.google.com do not put http://</span><br />
		<label>Text</label><input type="text" id="webName" /><span class="xsmall"> Ex: Google</span><br />
		<input type="submit" value="Insert"  id="createWeb"/>
	</p>
	<div class="clear"></div>
</div>
<div class="rounded main-box content-full">
	<?php
		if(!$advertisements)
		{
			echo 'You have no Advertisements at the moment';
		} else {
	?>
		<table class="tableClass small">
			<tr><th>Title</th><th>Body</th><th>Expires</th><th>Actions</th></tr>
			<?php
				foreach($advertisements as $ad)
				{
					if($ad['display']==0)
					{
						$class = 'style="background-color:#DDD"';
					} else {
						$class='';
					}
					echo '<tr '. $class.'><td><input type="hidden" name="adId" value="'.$ad['adId'].'"/>'.
						$ad['adTitle'].
						'</td><td>'.
						strip_tags(trim(substr($ad['adBody'],0,50))).'...'.
						'</td><td align="center">'.
						date('M, d, Y',strtotime($ad['adExpires'])).
						'</td><td align="center"><a href="'.base_url().'edit_advertisement/'.$ad['adId'].'">Edit</a> | <a href="#" class="deleteMe">Delete</a>';
						echo '<input type="hidden" name="display" id="display" value="'.$ad['display'].'" />';
						echo '</td></tr>';
				}
			?>
		</table>
	<?php
		}
	?>
</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".owners").addClass("active");
		$(".whats_happening").addClass("active");
		$(".classifieds").addClass("active");

	});

	$('.deleteMe').live("click", function(){
		$confirmDeletion = confirm("Are you sure you would like to delete this advertisement?");
		if ($confirmDeletion == true)
		{
			$row = $(this).closest('tr');
			$.ajax({
				type: "POST",
				url: "<?=base_url()?>admin/aj_delete_ad/",
				data: { adId: $row.find('input[type=hidden][name=adId]').val() },
				dataType: "json",
				success: function(content) {
					if (content.status == "success") {
						$row.hide();
					} else {
						$("#error").html('<p>'+content.message+'</p>');
					}
				}
			});
		}
		return false;
	});

	$("#submit").on("click",function(){
		var errors = '';
		if ($("#adTitle").val()=='')
			errors += '<li>You must enter a Title!</li>';
		if ($("#adBody").val()=='')
			errors += '<li>You must enter a Body!</li>';
		if ($("#adType").val()==0)
			errors += '<li>You must select a type of Ad!</li>';
		if(errors)
		{
			$("#error").html('<ul>' + errors + '</ul>');
			return false;
		} else {
			if($("#adPublic").val()==1)
				if(!confirm("Are you sure you want this ad displayed to the general public?"))
				{
					return false;
				} else {
					return true;
				}
		}
	});

	$("#createEmail").on("click", function(){
		if($("#emailLink").val())
		{

			var email = "<a href='mailto:"+$("#emailLink").val()+"'>"+$("#emailLink").val()+"</a>";
			$("#adBody").val($("#adBody").val() + " "+ email);
		}
	});
	$("#createWeb").on("click", function(){
		if($("#webLink").val())
		{

			var webLink = "<a href='http://"+$("#webLink").val()+"'' target='_blank'>"+$("#webName").val()+"</a>";
			$("#adBody").val($("#adBody").val() + " "+ webLink);
		}
	});
</script>
