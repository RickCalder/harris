<?php

	class Events extends MY_Model {
		protected $primaryKey = "eventId";
		public $table = "events";

		protected $eventId;
		protected $eventTitle;
		protected $eventData;
		protected $eventActive;
        protected $eventDisplay;
        protected $eventPublic;
		protected $eventDate;
        protected $eventPartyRoom;
        protected $createdBy;
        protected $requestEmail;

        function checkDate($date)
        {
            $this->db->where('eventDate',$date);
            $this->db->where('eventPartyRoom',1);
            $query = $this->db->get('events');
            if($query->num_rows() > 0)
            {
                return 1;
            } else {
                return 0;
            }

        }

		function getCalendarData($year, $month)
		{
			if(! $year)
			{
				$year = date('Y');
				$month = date('m');
			}

            $query = $this->db->query("SELECT DISTINCT DATE_FORMAT(eventDate,'%Y-%m-%d') AS date
                                                FROM events
                                                WHERE eventDate LIKE '$year-$month%' "); //date format eliminates zeros make
                                                                               //days look 05 to 5

            $cal_data = array();
            foreach ($query->result() as $row) { //for every date fetch data
                $a = array();
                $i = 0;
                $query2 = $this->db->query("SELECT eventTitle, eventId, eventPartyRoom, eventPublic
                                            FROM events
                                            WHERE eventDate LIKE '$row->date%' AND eventActive = 1 AND eventDisplay = 1");
                                                        //date format change back the date format
                                                        //that fetched earlier
                 foreach ($query2->result() as $r) {
                     $a[$i] = $r;     //make data array to put to specific date
                     $i++;
                 }
                    $cal_data[intval(substr($row->date,8,2))] = $a;

            }
            return $cal_data;
        }

        function getEvents($eventId = 0)
        {
            if($eventId > 0)
            {
                $this->db->where('eventId',$eventId);
            }
            $this->db->where('eventDisplay',1);
            $this->db->order_by('eventDate','DESC');
            $this->db->join('users','users.userId = events.createdBy','left');
            $data = $this->db->get('events');
            $result = array();
            $result = $data->result_array();
            return $result;
        }
	}
