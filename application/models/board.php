<?php

	class Board extends MY_Model {
		protected $primaryKey = "boardId";
		public $table = "board";		
	
		protected $boardId;	
		protected $boardFirstName;		
		protected $boardLastName;
		protected $boardTitle;
		protected $boardPhone;		
		protected $boardEmail;			
		protected $boardUnitNumber;
		protected $boardActive;	
		protected $priority;
		
		function getMembers()
		{
			$this->db->where('boardActive',1);
			$this->db->order_by('priority','ASC');
			$data = array();
			$data = $this->db->get('board');
			return $data->result_array();
		}
	}
	