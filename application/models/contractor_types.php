<?php
	class Contractor_types extends MY_Model {
		protected $primaryKey = "contractorTypeId";
		public $table = "contractor_types";

		protected $contractorTypeId;
		protected $contractorTypeName;
		protected $contractorTypeActive;

		function contractorDropdown()
		{
			$this->db->where('contractorTypeActive',1);
			$this->db->order_by('contractorTypeName');
			$query = $this->db->get('contractor_types');
			$data=array();
			$data['']="Contractor Type";
				if ($query->num_rows >= 1)
				{
					foreach($query->result_array() as $row)
					{
						$data[$row['contractorTypeId']]=$row['contractorTypeName'];
					}
					return $data;
				}
		}

		function getContractorTypes(){
			$this->db->where('contractorTypeActive',1);
			$this->db->order_by('contractorTypeName');
			$query = $this->db->get('contractor_types');
			if($query->num_rows >=1)
			{
				return $query->result();
			} else {
				return null;
			}

		}


	}

