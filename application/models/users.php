<?php

	class Users extends MY_Model {
		protected $primaryKey = "userId";
		public $table = "users";

		protected $userId;
		protected $userName;
		protected $unitNumber;
		protected $building;
		protected $emailAddress;
		protected $phone;
		protected $mobilePhone;
		protected $alternatePhone;
		protected $password;
		protected $passHash;
		protected $active;
		protected $creationDateTime;
		protected $updatedDateTime;


		function getUsers($userId=0,$building=0,$offset=0,$limit=0,$keyword = NULL)
		{
			if($userId > 0)
			{
				$this->db->where('users.userId',$userId);
			}
			if($building >0)
			{
				$this->db->where('users.building',$building);
			}
			if($keyword)
			{
				$this->db->where('people.firstName LIKE','%'.$keyword.'%');
				$this->db->or_where('people.lastName LIKE','%'.$keyword.'%');
			}
			$this->db->where('users.active',1);
			$this->db->join('people','people.userId=users.userId');
			$this->db->group_by('users.userName');
			$this->db->order_by('users.building asc, users.unitNumber asc');
			$data=array();
			if($limit !=0)
			{
				$this->db->limit($limit, $offset);
			}
			$data = $this->db->get('users');
			return $data->result_array();
		}

		function getUser($userId)
		{
			$this->db->where('users.userId',$userId);
			$this->db->where('users.active',1);
			$this->db->where('people.active',1);
			$this->db->join('people','people.userId=users.userId');
			$data = $this->db->get('users');
			return $data->result_array();

		}


		function getPeopleByUnit()
		{
			$this->load->model('people');
			$this->db->where('people.userId',$this->userId);
			$data = $this->db->get('people');
			$this->person = $data->result();
			return $this->person;
		}
		/**
			Static function used to encrypt the passwords for the system.
		*/
		public static function EncryptPassword($password) {
			$password = sha1($password);
			return $password;
		}

	}
