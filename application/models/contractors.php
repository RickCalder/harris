<?php
	class Contractors extends MY_Model {
		protected $primaryKey = "contractorId";
		public $table = "contractors";

		protected $contractorId;
		protected $contractorType;
		protected $contractorName;
		protected $contractorPhone;
		protected $contractorDescription;
		protected $contractorActive;

		public function getContractors($contractorId,$active,$contractorType)
		{
			if($contractorId)
			{
				$this->db->where('contractorId',$contractorId);
			}
			if($contractorType)
			{
				$this->db->where('contractorType',$contractorType);
			}
			if($active)
			{
				$this->db->where('contractorActive',$active);
			}
			$this->db->where('contractorActive <>',999);
			$this->db->join('contractor_types','contractor_types.contractorTypeId = contractors.contractorType');
			$this->db->where('contractorTypeActive',1);
			$data = $this->db->get('contractors');
			return $data->result();

		}

	}

