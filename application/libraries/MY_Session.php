<?php
class MY_Session extends CI_Session {

	function __construct()
	{
		parent::__construct();
	}

	///////////////////////////////////////////////////////////////////////////////
	// Validates username and password info then begins the session              //
	///////////////////////////////////////////////////////////////////////////////
	function login($user,$pass)
	{
		if ($this->CI->config->item('sess_encrypt_cookie') == TRUE AND $this->CI->config->item('sess_use_database') == TRUE AND $this->CI->config->item('sess_table_name') != '')
		{
			$this->CI->load->model("users");
			// Validate the username and password.
			$this->CI->load->database();
			
			$this->CI->db->where("userName", $user);
			$this->CI->db->where("password", Users::EncryptPassword($pass));
			$this->CI->db->where("active", 1);
			$query = $this->CI->db->get("users");
			
			if ($query->num_rows() == 1) {
				foreach ($query->result_array() as $row)
				{
					$userdata = array();
					$userdata['isLoggedIn'] = TRUE;
					$userdata['userId'] = $row['userId'];
					
					$roles = $this->loadRoles($row['userId']);
					
					$userdata['roleNames'] = array_values($roles);
					$userdata['roleIds'] = array_keys($roles);

					
					$this->set_userdata($userdata);
          redirect('owners_home');
					return true;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			show_error('ENCRYPTION AND DATABASE MUST BE ENABLED - PLEASE READ /APPLICATION/CONFIG/AUTH.PHP');
			return false;
		}
	}
	///////////////////////////////////////////////////////////////////////////////
	// Removes the session authorization and user name from the client           //
	///////////////////////////////////////////////////////////////////////////////
	function logout()
	{
		// Empty all user data.
		$this->CI->session->sess_destroy();

	}
	
	/**
		Checks to see if this user has access to a role, using role name as the key.
	*/
	function hasRoleByName($roleName)
	{
		return in_array($roleName, $this->userdata('roleNames'));
	}
	
	/**
		Loads the roles from the database for this user.
	*/
	private function loadRoles($userId) {
		$select = "
			SELECT *
			FROM user_roles ur, roles r 
			WHERE ur.roleId = r.roleId 
			AND ur.userId = " . $userId;
		
		$this->CI->load->database();
		$rslt = $this->CI->db->query($select);
		
		$roles = array();
		
		foreach ($rslt->result_array() as $row) {
			$roles[$row['roleId']] = $row['roleKey'];
			
			// If they have access to admin_basic, give them access to the Admin Home Controller.
			if ($row['roleKey'] == PRIV_ADMIN_BASIC) {
				$roles[0] = "Home";
			}
		}
		
		return $roles;
	}
	
	public function addMessage($msg) {
		$msgs = $this->userdata("messages");
		//debug(count($msgs));
		
		if ($msgs == null) {
			$msgs = array();
		}
		
		if(! in_array($msg, $msgs))
		{
			$msgs[] = $msg;	
		}
		
		$this->set_userdata("messages", $msgs);
	}
	
	public function getMessages() {
		$msgs = $this->userdata("messages");
		$this->unset_userdata("messages");
		
		return $msgs;
	}
	
	public function addError($error) {
		$errors = $this->userdata("errors");
		//debug(count($msgs));
		
		if ($errors == null) {
			$errors = array();
		}
		
		if(! in_array($error, $errors))
		{
			$errors[] = $error;		
		}
		
		$this->set_userdata("errors", $errors);
	}
	
	public function getErrors() {
		$errors = $this->userdata("errors");
		$this->unset_userdata("errors");
		
		return $errors;
	}
}