<div class="content">
	<h2 class="top">Building Contact</h2>
	<div class="main-box rounded">
		<p class="top">Harris Towers<br />108-30 Harrisford Street<br />Hamilton, ON&nbsp;&nbsp;L8K 6N1<p>
		<p><strong>Telephone:</strong> <a href="tel:9055605536">905-560-5536</a>
		<p><strong>Email:</strong>  <a href="mailto:harristowers@rogers.com">harristowers@rogers.com</a></p>
	</div>
	<h2>Property Management</h2>
	<div class="main-box rounded">
		<p class="top">Pat Kummer<br />Property Manager<br />Precision Management Services Inc.<br />33 King Street East, Suite 9<br />Dundas, ON&nbsp;&nbsp;L9H 1B7<p>
		<p><strong>Telephone:</strong> <a href="tel:905-544-0077">905-544-0077</a></p>
		<p><strong>Fax:</strong> 905-627-3530</p>
		<p><strong>Email:</strong> <a href="mailto:precision@cogeco.net">precision@cogeco.net</a></p>
	</div>
	<div class="center">
		<iframe width="675" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=30+Harrisford+Street,+Hamilton,+ON,+Canada&amp;aq=0&amp;oq=30+harrisford+st&amp;sll=37.0625,-95.677068&amp;sspn=57.553742,79.013672&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=30+Harrisford+St,+Hamilton,+Ontario+L8K+6N1,+Canada&amp;z=14&amp;ll=43.21895,-79.796096&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=30+Harrisford+Street,+Hamilton,+ON,+Canada&amp;aq=0&amp;oq=30+harrisford+st&amp;sll=37.0625,-95.677068&amp;sspn=57.553742,79.013672&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=30+Harrisford+St,+Hamilton,+Ontario+L8K+6N1,+Canada&amp;z=14&amp;ll=43.21895,-79.796096" style="color:#0000FF;text-align:left">View Larger Map</a></small>
	</div>

</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".contact").addClass("active");
	});
</script>

