<div class="content content-full">
<div class="rounded main-box top content-full">	

<div class="right" style="width:400px;">
	<h3>Instructions</h3>
	<ul class="small">
		<li>Gallery name must be unique, try to be descriptive but not too wordy. Ie Christmas Party 2013 is fine.</li>
		<li>Only images can be uploaded, files must end in .gif, .jpg or .png only, any other file types will be rejected.</li>
		<li>Images are limited to 4 megabytes in size, anything larger will be rejected as well.</li>
		<li>Images uploaded from iPhones can have rotation issues with the thumbnail!</li>
	</ul>
	</div>
	
	<h3>Add/Edit Image Gallery</h3>
	<div class="form_errors"><?=validation_errors();?></div>
	<?php 
		$galleryDrop = array();
		$galleryDrop['x']='--Select Gallery--';
		$galleryDrop[0]='--Add New--';
		foreach ($galleries as $gallery)
		{
			if($gallery['galleryActive']==1)
			{
				$galleryDrop[$gallery['galleryId']]=$gallery['galleryName'];
			}
		}
		$attributes=array('name'=>'imageGallery','class'=>'createForm','enctype'=>'multipart/form-data');
		echo form_open('admin_image_gallery',$attributes);
		echo '<p>';
		echo form_label('Gallery Name: ','galleryName');
		echo form_dropdown('galleryId',$galleryDrop,set_value('galleryId'),'id=galleryId');
		echo '<br />';
		echo form_label('','blank');
		$formData = array(
			'name'=>'galleryName',
			'id'=>'galleryName',
			'style'=>'display:none;',
			'value'=>set_value('galleryName')
		);
		echo form_input($formData);
		echo '</p>';				
		echo '<table id="imageTable">';		
		echo '<tr id="cloneLine"><td>';		
		echo form_label('Image','');		
		echo '</td><td>';		
		echo '<input type="file" name="image[]" class="fileInput" />';		
		echo '</tr>';		
		echo '</table>';				
		echo '<p class="center" style="margin-top:25px;">';
		$formData = array(
			'name'=>'submit',
			'id'=>'submit',
			'class'=>'button',
			'value'=>'Upload Images'
		);
		echo form_submit($formData);
		echo form_close();
	?>	
	<div class="clear"></div>
</div>
<div class="rounded main-box top content-full">	
	<h3>Galleries</h3>
	<table class="tableClass">
	<tr><th>Gallery Name</th><th>Number of Images</th><th>Created</th><th></th></tr>
		<?php
			echo form_open('galleries');
			foreach($galleries as $gallery)
			{
				echo '<tr>';
				echo '<td>';
				echo '<input type="hidden" name="galleryId" id="galleryId" value="'.$gallery['galleryId'].'" ></input>';
				echo '<input type="hidden" name="galleryActive" id="galleryActive" value="'.$gallery['galleryActive'].'" ></input>';
				echo $gallery['galleryName'].'</td>';
				echo '<td align="center">'.$gallery['imageCount'].'</td>';
				echo '<td align="center">'.Date('M d, Y',strtotime($gallery['date'])).'</td>';
				echo '<td align="right">';
				echo $gallery['galleryActive'] == 1 ? '<a href="" id="activate" class="activate">Active</a>' : '<a href="" id="activate" class="activate">Inactive</a>';
				echo '&nbsp;|&nbsp;<a href="'.base_url().'admin_view_gallery/'.$gallery['galleryId'].'">View/Edit</a>';
				echo '&nbsp;|&nbsp;<a href="#" class="deleteLink">Delete</a>';
				echo '</td>';
				echo '</tr>';
			}
			echo form_close();
		?>
	</table>
</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".admin").addClass("active");
		$(".adminImageGallery").addClass("active");
	});
	
	$("#submit").on("click", function(){	
		var errors = '';	
		
		
		//checkName();
		$.ajax({
			type: "GET",
			url: "<?=base_url()?>admin/checkGalleryName/",
			data: { galleryName: $('#galleryName').val(), galleryId: $("#galleryId").val()},
			async:false,
			dataType: "json",
			success: function(content) {
				if (content.status == "error") {
					errors += "<li>That Gallery Name already exists, please choose another!</li>";	
				}  	
				if($("#galleryName").val()=="")		
				{			
					errors +="<li>You must enter a gallery name</li>";		
				}		
				if($(".fileInput").val()=="")		
				{			
					errors +="<li>You must choose at least one image</li>";		
				}	
			}
		});
			
		if(errors != '')		
		{			
			$(".form_errors").html(errors);
			return false;		
		}	
	});		
	
	$(".fileInput").on("change",function(){
		$row = $(this).closest('tr');
		if(this.files[0].size > 4096000)
		{
			$("#cloneLine").clone(true).insertAfter("#imageTable tbody>tr:last");
			$row.html("");
			$(".form_errors").html("Files must be smaller than 4MB, that file was "+ (this.files[0].size/1000000).toFixed(0) +"MB");
			return false;
		}
		var type = $(this).val();
		switch(type.substring(type.lastIndexOf('.')+1).toLowerCase())
		{
			case "gif" : case "jpg" : case "png" :
				$(".form_errors").html("");			
				break;
			default :
			$("#cloneLine").clone(true).insertAfter("#imageTable tbody>tr:last");
			$row.html("");
			$(".form_errors").html("Files must be an image, only gif, jpg or png allowed!");
			return false;
				break;
		}		
		
		if ($row.is($("#imageTable tbody>tr:last-child"))) 
		{
			$("#cloneLine").clone(true).insertAfter("#imageTable tbody>tr:last");		
		}	
	});
	
	$(".activate").on("click",function(e){
		e.preventDefault();
		$row = $(this).closest("tr");
		$.ajax({
			type: "GET",
			async: false,
			url: "<?=base_url()?>admin/galleryActive/",
			data: $row.find('input').serialize(),
			dataType: "json",
			success: function(content) {
				if (content.status == "success") {
					$("#galleryActive", $row).val(content.message);
					if(content.message==1)
					{
						$("#activate",$row).html("Active");
					} else {
						$("#activate",$row).html("Inactive");
					}
				}  else {
				
				}
			}
		});
		return false;
	});
	
	$("#galleryId").on("change", function() {
		if($(this).val()==0)
		{
			$("#galleryName").val("");
			$("#galleryName").show();
			$("#galleryName").removeAttr("disabled");
		} else if($(this).val()=='x') 
		{
			$("#galleryName").val("");
			$("#galleryName").hide();
		} else {
			$("#galleryName").val($("#galleryId option:selected").text());
			$("#galleryName").hide();
		}
		
	});
	
	
	$(".deleteLink").on("click",function(){
		if(!confirm('Are you sure you want to delete this gallery?'))
		return false;
		$row = $(this).closest('tr');
		$.ajax({
			type: "POST",
			url: "<?=base_url()?>admin/aj_galleryDelete",
			data: $row.find('input').serialize(),
			dataType: "json",
			success: function(content) {
				window.location="<?=base_url();?>admin_image_gallery";
			}
		});
		return false;
	});
</script>
