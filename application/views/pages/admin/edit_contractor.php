<div class="content content-full">
<div class="rounded main-box top content-full">
	<h3>Contractors</h3>
	<div class="form_errors" id="error"><?=validation_errors();?></div>
	<?php //debug($advertisement);
		$attributes=array('name'=>'contractors','class'=>'formClass');
		echo form_open(base_url().'admin/edit_contractor',$attributes);
		echo '<input type="hidden" name="contractorActive" value="1" />';
		echo '<input type="hidden" name="contractorId" value="'.$contractor[0]->contractorId.'" />';
		echo '<p style="margin-bottom:5px;">';
		echo form_label('Name: ','name');
		$formData = array(
			'name'=>'contractorName',
			'id'=>'contractorName',
			'value'=>set_value('contractorName') ? set_value('contractorName') : $contractor[0]->contractorName		);
		echo form_input($formData);
		echo '</p><p style="margin-bottom:5px;">';

		echo '<p style="margin-bottom:5px;">';
		echo form_label('Type: ','contractorType');
		echo form_dropdown('contractorType',$contractorTypes,set_value('contractorType') ? set_value('contractorType') : $contractor[0]->contractorType,'id="contractorType"');
		echo '</p><p style="margin-bottom:5px;">';

		echo '</p><p class="textareabox" style="margin-bottom:5px;">';
		echo form_label('Description: ','contractorDescription');
		$formData = array(
			'name'=>'contractorDescription',
			'id'=>'contractorDescription',
			'style'=>'height:125px;',
			'value'=>set_value('contractorDescription') ? set_value('contractorDescription') : $contractor[0]->contractorDescription
			);
		echo form_textarea($formData);
		echo '</p><p style="margin-bottom:5px;">';
		echo form_label('Phone: ','contractorPhone');
		$formData = array(
			'name'=>'contractorPhone',
			'id'=>'contractorPhone',
			'value'=>set_value('contractorPhone') ? set_value('contractorPhone') : $contractor[0]->contractorPhone
		);
		echo form_input($formData);
		echo '</p><p style="margin-bottom:5px;">';
		echo '</p><p style="width:150px;margin-left:250px;">';
		$formData = array(
			'name'=>'submit',
			'id'=>'submit',
			'class'=>'button',
			'value'=>'Submit'
		);
		echo form_submit($formData);
		echo form_close();
	?>

</div>
</div>
<script type="text/javascript">


	$(document).ready(function(){
		$(".admin").addClass("active");
		$(".notifications").addClass("active");
		$(".admin_contractors").addClass("active");

        });

</script>
