<div class="content">
<h2 class="top">Documents</h2>
<p>
	<label for="type-select">Filter</label>
	<select id="type-select">
		<option value="all">View All</option>
		<option value="newsletter">Newsletters</option>
		<option value="pic">PICs</option>
	</select>
</p>
<table class="tableClass">
<?php
	foreach($newsletters as $newsletter)
	{
		if($newsletter['active']==1)
		{
			$active='Active';
		} else if($newsletter['active']==0){
			$active = 'Disabled';
		}

		if($newsletter['type'] === 'newsletter') {
			$link = '<td width="50"><a href="'.base_url().'newsletters/'.$newsletter['newsletterId'].'">View</a></td>';
		} else {
			$link = '<td width="50"><a href="'.base_url().'periodics/'.$newsletter['newsletterId'].'">View</a></td>';
		}
		echo '<tr class="item-row '.$newsletter['type'].'"><td>'.$newsletter['newsletterHeading'].'</td><td>'.date('M, d, Y',strtotime($newsletter['creationDateTime'])).'</td>'. $link.
		'<td width="50"><a href="'.base_url().'newsletter_edit/'.$newsletter['newsletterId'].'">Edit</a></td>'.
		'<td width="50"><input type="hidden" name="newsletterId" value="'.$newsletter['newsletterId'].'" /><input type="hidden" id="active" name="active" value="'.$newsletter['active'].'" /><a href="" class="active" id="'.$newsletter['newsletterId'].'">'.$active.'</a></td>'.
		'<td width="75" align="right"><a href="" class="delete">Delete</a></td>'.
		'</td></tr>';
	}
?>
</table>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".admin").addClass("active");
		$(".newsletter_list").addClass("active");
		$(".newslettersmenuadmin").addClass("active");
	});
	$(".active").on("click",function(){
		$row = $(this).closest('tr');
		$.ajax({
			type: "GET",
			url: "<?=base_url()?>admin/aj_newsletterViewable",
			data: $row.find('input').serialize(),
			dataType: "json",
			success: function(content) {
				if (content.status == "success") {
					if(content.message==0)
					{
						$("#active",$row).val(0);
						$(".active",$row).html("Disabled");
					} else {
						$("#active",$row).val(1);
						$(".active",$row).html("Active");
					}
				}
			}
		});
		return false;
	});
	
	$(".delete").on("click",function(){
		if(!confirm('Are you sure you want to delete this newsletter?'))
		return false;
		$row = $(this).closest('tr');
		$.ajax({
			type: "GET",
			url: "<?=base_url()?>admin/aj_newsletterDelete",
			data: $row.find('input').serialize(),
			dataType: "json",
			success: function(content) {
				window.location="<?=base_url();?>newsletter_list";
			}
		});
		return false;
	});

	$("#type-select").on("change", function(){
		$(".item-row").hide();
		if($(this).val() === 'all') {
			$(".item-row").show()
		} else if($(this).val() === 'pic') {
			$(".item-row").each(function() {
				if($(this).hasClass('pi')) {
					$(this).show();
				}
			})
		} else {
			$(".item-row").each(function() {
				if($(this).hasClass('newsletter')) {
					$(this).show();
				}
			})
		}

	})
</script>
