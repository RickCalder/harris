<div class="content content-full">
<h2>Users</h2>
<div class="main-box rounded content-full">
<h3 class="top">Filter</h3>
<?php
	$attributes=array('class'=>'searchForm');
	echo form_open('users',$attributes);
	echo '<p>';
	echo form_label('Building','building');
	$formdata = array(
		'0'=>'Both',
		'30'=>'Thirty',
		'40'=>'Forty'
	);
	echo form_dropdown('building',$formdata,$this->session->userdata('building'));
	echo '</p><p>';
	echo form_label('Search','keyword');
	$formdata=array(
		'name'=>'keyword',
		'id'=>'keyword',
		'value'=>$this->session->userdata('keyword')
	);
	echo form_input($formdata);
	echo '</p><p>';
	$formdata=array(
		'name'=>'submit',
		'class'=>'button',
		'value'=>'Search'
	);
	echo form_submit($formdata);
	$formdata=array(
		'name'=>'clear',
		'class'=>'button',
		'value'=>'Clear Filters'
	);
	echo form_submit($formdata);
	echo '</p>';
	echo form_close();
?>
</div>
<div class="links">
	<?=$links;?>
</div>	
<table class="tableClass">
<tr><th></th><th>Name</th><th>Email</th><th>Phone</th><th></th></tr>
<?php	foreach($users as $user)
	{
		echo '<tr>';
		echo '<td>'.$user->get('building').' - '.$user->get('unitNumber').'</td>';
		$people = $user->getPeopleByUnit();
		echo '<td>';
		foreach ($people as $person)
		{
			echo $person->firstName.' '.$person->lastName.'<br />';
		}		echo '</td>';
		echo '<td><a href="mailto:'.$user->get('emailAddress').'">'.$user->get('emailAddress').'</a></td>';
		echo '<td align="center">'.$user->get('phone').'</td>';
		echo '<td align="center"><a href="'.base_url().'admin/edit_user/'.$user->get('userId').'">Edit</a></td>';
		echo '</tr>';
	}?></table>
<div class="links">
	<?=$links;?>
</div>	
</div><script type="text/javascript">
	$(document).ready(function(){
		$(".admin").addClass("active");
		$(".users").addClass("active");
	});
</script>
