<div class="content content-full">
<h2>Create User</h2>
<div class="main-box rounded content-full"><div class="form_errors">	<?=validation_errors();?></div>
<?php
	$attributes=array('class'=>'createForm');
	echo form_open('new_user',$attributes);
	echo '<p>';
	echo form_label('Building','building');
	$formdata = array(
		'30'=>'Thirty',
		'40'=>'Forty',		'99'=>'Off Site'
	);
	echo form_dropdown('building',$formdata,$this->session->userdata('building'));
	echo '</p><p>';
	echo form_label('Unit Number *','unitNumber');
	$formdata=array(
		'name'=>'unitNumber',
		'id'=>'unitNumber',
		'value'=>set_value('unitNumber')
	);	echo form_input($formdata);		echo '</p><p>';	echo '<p>';	echo form_label('Salutation','Salutation');	$formdata = array(		'Mr.'=>'Mr.',		'Mrs.'=>'Mrs.',		'Ms.'=>'Ms.',		'Miss'=>'Miss',	);	echo form_dropdown('Salutation',$formdata,set_value('salutation'));	echo '</p><p>';		echo form_label('First Name *','firstName');	$formdata=array(		'name'=>'firstName',		'id'=>'firstName',		'value'=>set_value('firstName')	);
	echo form_input($formdata);		echo '</p><p>';		echo form_label('Last Name *','lastName');	$formdata=array(		'name'=>'lastName',		'id'=>'lastName',		'value'=>set_value('lastName')	);	echo form_input($formdata);		echo '</p><p>';		echo form_label('User Name *','userName');	$formdata=array(		'name'=>'userName',		'id'=>'userName',		'value'=>set_value('userName')	);	echo form_input($formdata);		echo '</p><p>';		echo form_label('Email','emailAddress');	$formdata=array(		'name'=>'emailAddress',		'id'=>'emailAddress',		'value'=>set_value('emailAddress')	);	echo form_input($formdata);		echo '</p><p>';		echo form_label('Phone *','phone');	$formdata=array(		'name'=>'phone',		'id'=>'phone',		'value'=>set_value('phone')	);	echo form_input($formdata);		echo '</p><p>';		echo form_label('Mobile Phone','mobilePhone');	$formdata=array(		'name'=>'mobilePhone',		'id'=>'mobilePhone',		'value'=>set_value('mobilePhone')	);	echo form_input($formdata);		echo '</p><p>';		echo form_label('Alternate Phone','alternatePhone');	$formdata=array(		'name'=>'alternatePhone',		'id'=>'alternatePhone',		'value'=>set_value('alternatePhone')	);	echo form_input($formdata);		echo '</p><p>';		echo form_label('Password *','password');	$formdata=array(		'name'=>'password',		'id'=>'password',		'autocomplete'=>'off'	);	echo form_password($formdata);		echo '</p><p>';		echo form_label('Verify Password *','verifyPassword');	$formdata=array(		'name'=>'verifyPassword',		'id'=>'verifyPassword',		'autocomplete'=>'off'	);	echo form_password($formdata);	
	echo '</p><p class="center">';
	$formdata=array(
		'name'=>'submit',
		'class'=>'button',
		'value'=>'Create User'
	);
	echo form_submit($formdata);
	echo '</p>';
	echo form_close();
?>
</div>	
</div><script type="text/javascript">
	$(document).ready(function(){
		$(".admin").addClass("active");
		$(".new_user").addClass("active");
	});
</script>
