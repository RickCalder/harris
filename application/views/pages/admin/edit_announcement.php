<div class="content content-full">
<div class="rounded main-box top content-full">
	<div class="right" style="width:400px;">
	<h3>Instructions</h3>
	<p class="small">
		The body of the announcements can to take some basic HTML to help you format your message and highlight important parts of the message. <br />You can use the following commands:<br /><br />
		
		&lt;strong&gt;<strong>Bold text</strong>&lt;/strong&gt;<br />
		<em>&lt;em&gt;Italic text&lt;/em&gt;</em><br />
		<u>&lt;u&gt;Underlined text&lt;/u&gt;</u><br />
		Line break&lt;br /&gt;<br />
	</p>
	</div>
	
	<h3>Editing Announcement #<?=$announcement[0]['announcementId'];?></h3>
	<div class="form_errors"><?=validation_errors();?></div>
	<?php 
		$attributes=array('name'=>'announcementForm','class'=>'formClass');
		echo form_open(base_url().'admin/edit_announcement',$attributes);
		echo '<input type="hidden" name="active" value="1" />';
		echo '<input type="hidden" name="createdBy" value="'.$this->session->userdata('userId').'" />';
		echo '<input type="hidden" name="announcementId" value="'.$announcement[0]['announcementId'].'" />';
		echo '<p style="margin-bottom:5px;">';
		echo form_label('Title: ','title');
		$formData = array(
			'name'=>'announcementTitle',
			'id'=>'title',
			'value'=>$announcement[0]['announcementTitle']
		);
		echo form_input($formData);
		echo '</p><p style="margin-bottom:5px;">';
		echo '</p><p class="textareabox" style="margin-bottom:5px;">';
		echo form_label('Body: ','body');
		$formData = array(
			'name'=>'announcementBody',
			'id'=>'body',
			'style'=>'height:125px;',
			'value'=>$announcement[0]['announcementBody']
		);
		echo form_textarea($formData);	
		echo '<p style="margin-bottom:5px;">';
		echo form_label('Start Date: ','displayStart');
		echo '<input type="text" name="displayStart" id="displayStart" value="'.($announcement[0]['displayStart'] ? date('Y-m-d',strtotime($announcement[0]['displayStart'])) : Date("Y-m-d")).'"/>';
		echo '</p><p style="margin-bottom:5px;">';
		echo '<p style="margin-bottom:5px;">';
		echo form_label('End Date: ','displayUntil');
		echo '<input type="text" name="displayUntil" id="displayUntil" value="'.($announcement[0]['displayUntil'] ? date('Y-m-d',strtotime($announcement[0]['displayUntil'])) : date('Y-m-d', strtotime('+1 Week'))).'"/>';
		echo '</p><p style="width:150px;margin-left:250px;">';
		$formData = array(
			'name'=>'submit',
			'id'=>'submit',
			'class'=>'button',
			'value'=>'Submit'
		);
		echo form_submit($formData);
		echo form_close();
	?>	
</div>

</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".admin").addClass("active");
		$(".announcements").addClass("active");
		
		$( "#displayStart, #displayUntil" ).datepicker({
				firstDay:0,
				dateFormat:"yy-m-d",
				constrainInput:true
			});
	});
</script>
