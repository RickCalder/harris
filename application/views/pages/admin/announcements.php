<div class="content content-full">
<div class="rounded main-box top content-full">
	<div class="right" style="width:400px;">
	<h3>Instructions</h3>
	<p class="small">
		The body of the announcements can take some basic HTML to help you format your message and highlight important parts of the message. <br />You can use the following commands:<br /><br />
		&lt;strong&gt;<strong>Bold text</strong>&lt;/strong&gt;<br />
		<em>&lt;em&gt;Italic text&lt;/em&gt;</em><br />
		<u>&lt;u&gt;Underlined text&lt;/u&gt;</u><br />
		Line break&lt;br /&gt;<br />
	</p>
	</div>
	<h3>Announcements</h3>
	<div class="form_errors"><?=validation_errors();?></div>
	<?php
		$attributes=array('name'=>'announcementForm','class'=>'formClass');
		echo form_open(base_url().'admin/announcements',$attributes);
		echo '<input type="hidden" name="active" value="1" />';
		echo '<input type="hidden" name="createdBy" value="'.$this->session->userdata('userId').'" />';
		echo '<p style="margin-bottom:5px;">';
		echo form_label('Title: ','title');
		$formData = array(
			'name'=>'announcementTitle',
			'id'=>'title',
			'value'=>set_value('announcementTitle')
		);
		echo form_input($formData);
		echo '</p><p style="margin-bottom:5px;">';
		echo '</p><p class="textareabox" style="margin-bottom:5px;">';
		echo form_label('Body: ','body');
		$formData = array(
			'name'=>'announcementBody',
			'id'=>'body',
			'style'=>'height:125px;',
			'value'=>set_value('announcementBody')
		);
		echo form_textarea($formData);
		echo '<p style="margin-bottom:5px;">';
		echo form_label('Start Date: ','displayStart');
		echo '<input type="text" name="displayStart" id="displayStart" value="'.(set_value('displayStart') ? set_value('displayStart') : Date("Y-m-d")).'"/>';
		echo '</p><p style="margin-bottom:5px;">';
		echo '<p style="margin-bottom:5px;">';
		echo form_label('End Date: ','displayUntil');
		echo '<input type="text" name="displayUntil" id="displayUntil" value="'.(set_value('displayUntil') ? set_value('displayUntil') : date('Y-m-d', strtotime('+1 Week'))).'"/>';
		echo '</p><p style="width:150px;margin-left:250px;">';
		$formData = array(
			'name'=>'submit',
			'id'=>'submit',
			'class'=>'button',
			'value'=>'Submit'
		);
		echo form_submit($formData);
		echo form_close();
	?>
</div>
<div class="rounded main-box content-full">
	<?php
		if(!$announcements)
		{
			echo 'There are no announcements at the moment';
		} else {
	?>
		<table class="tableClass small">
			<tr><th>Title</th><th>Body</th><th>Start</th><th>End</th><th>Actions</th></tr>
			<?php
				foreach($announcements as $announce)
				{
					echo '<tr><td><input type="hidden" name="announcementId" value="'.$announce['announcementId'].'"/>'.
						$announce['announcementTitle'].
						'</td><td>'.
						strip_tags(trim(substr($announce['announcementBody'],0,50))).'...'.
						'</td><td align="center">'.
						date('M, d, Y',strtotime($announce['displayStart'])).
						'</td><td align="center">'.
						date('M, d, Y',strtotime($announce['displayUntil'])).
						'</td><td align="center"><a href="'.base_url().'edit_announcement/'.$announce['announcementId'].'">Edit</a> | <a href="#" class="deleteMe">Delete</a>'.
						'</td><td></tr>';
				}
			?>
		</table>
	<?php
		}
	?>
</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".admin").addClass("active");
		$(".notifications").addClass("active");
		$(".admin_announcements").addClass("active");
		$( "#displayStart, #displayUntil" ).datepicker({
				firstDay:0,
				dateFormat:"yy-m-d",
				constrainInput:true
			});
	});
	$('.deleteMe').live("click", function(){
		$confirmDeletion = confirm("Are you sure you would like to delete this announcement?");
		if ($confirmDeletion == true)
		{
			$row = $(this).closest('tr');
			$.ajax({
				type: "POST",
				url: "<?=base_url()?>admin/aj_delete_announce/",
				data: { announcementId: $row.find('input[type=hidden][name=announcementId]').val() },
				dataType: "json",
				success: function(content) {
					if (content.status == "success") {
						$row.hide();
					} else {
						$("#error").html('<p>'+content.message+'</p>');
					}
				}
			});
		}
		return false;
	});
</script>
