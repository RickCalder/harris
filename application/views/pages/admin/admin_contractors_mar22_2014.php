<div class="content content-full">
<div class="rounded main-box top content-full">
	<h3>Contractors</h3>
	<div class="form_errors" id="error"><?=validation_errors();?></div>
	<?php //debug($advertisement);
		$attributes=array('name'=>'contractors','class'=>'formClass');
		echo form_open(base_url().'admin/admin_contractors',$attributes);
		echo '<input type="hidden" name="contractorActive" value="1" />';
		echo '<p style="margin-bottom:5px;">';
		echo form_label('Name: ','name');
		$formData = array(
			'name'=>'contractorName',
			'id'=>'contractorName',
			'value'=>set_value('contractorName')
		);
		echo form_input($formData);
		echo '</p><p style="margin-bottom:5px;">';

		echo '<p style="margin-bottom:5px;">';
		echo form_label('Type: ','contractorType');
		echo form_dropdown('contractorType',$contractorTypes,set_value('contractorType'),'id="contractorType"');
		echo '</p><p style="margin-bottom:5px;">';

		echo '</p><p class="textareabox" style="margin-bottom:5px;">';
		echo form_label('Description: ','contractorDescription');
		$formData = array(
			'name'=>'contractorDescription',
			'id'=>'contractorDescription',
			'style'=>'height:125px;',
			'value'=>set_value('contractorDescription')
			);
		echo form_textarea($formData);
		echo '</p><p style="margin-bottom:5px;">';
		echo form_label('Phone: ','contractorPhone');
		$formData = array(
			'name'=>'contractorPhone',
			'id'=>'contractorPhone',
			'value'=>set_value('contractorPhone')
		);
		echo form_input($formData);
		echo '</p><p style="margin-bottom:5px;">';
		echo '</p><p style="width:150px;margin-left:250px;">';
		$formData = array(
			'name'=>'submit',
			'id'=>'submit',
			'class'=>'button',
			'value'=>'Submit'
		);
		echo form_submit($formData);
		echo form_close();
	?>

</div>
<div class="rounded main-box content-full">
	<?php
		if(!$contractors)
		{
			echo 'There are no Contractors at the moment';
		} else {
	?>
		<table class="tableClass small">
			<tr><th>Type</th><th>Name</th><th>Description</th><th>Phone</th><th>Actions</th></tr>
			<?php
				foreach($contractors as $contractor)
				{

					if($contractor->contractorActive == 0)
					{
						$class = ' style="background-color:#DDD"';
					} else {
						$class='';
					}
					echo '<tr'.$class.'>
						<td>'.$contractor->contractorTypeName.'
						<input type="hidden" name="contractorActive" value="'.$contractor->contractorActive.'" id="contractorActive" />
						</td>
					<td><input type="hidden" name="contractorId" value="'.$contractor->contractorId.'"/>'.
						$contractor->contractorName.
						'</td><td width="375">'.
						$contractor->contractorDescription.
						'</td><td align="center">'.
						$contractor->contractorPhone.
						'</td><td align="center"><a href="'.base_url().'edit_contractor/'.$contractor->contractorId.'">Edit</a> | <a href="#" class="deleteMe">Delete</a>&nbsp;|&nbsp;';
						echo $contractor->contractorActive == 1 ? '<a href="" id="activate" class="activate">Hide</a>' : '<a href="" id="activate" class="activate">Display</a>';
						echo '</td></tr>';
				}
			?>
		</table>
	<?php
		}
	?>
</div>
</div>
<script type="text/javascript">

	$(".activate").on("click",function(){
		$row = $(this).closest('tr');
		$.ajax({
			type: "POST",
			url: "<?=base_url()?>admin/aj_contractor_display/",
			data:$row.find('input').serialize(),
			dataType: "json",
			success: function(content) {
				if (content.status == "success") {
					$("#contractorActive", $row).val(content.message);
					if(content.message==1)
					{
						$(".activate",$row).html("Hide");
						$row.css("background-color","#FFF");
					} else {
						$(".activate",$row).html("Display");
						$row.css("background-color","#DDD");
					}
				} else {
					$("#error").html('<p>'+content.message+'</p>');
				}
			}
		});
		return false
	});

	$('.deleteMe').live("click", function(){
		$confirmDeletion = confirm("Are you sure you would like to delete this contractor?");
		if ($confirmDeletion == true)
		{
			$row = $(this).closest('tr');
			$.ajax({
				type: "POST",
				url: "<?=base_url()?>admin/aj_delete_contractor/",
				data: { contractorId: $row.find('input[type=hidden][name=contractorId]').val() },
				dataType: "json",
				success: function(content) {
					if (content.status == "success") {
						$row.hide();
					} else {
						$("#error").html('<p>'+content.message+'</p>');
					}
				}
			});
		}
		return false;
	});


	$(document).ready(function(){
		$(".admin").addClass("active");
		$(".notifications").addClass("active");
		$(".admin_contractors").addClass("active");

        });

</script>
