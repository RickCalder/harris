<div class="content content-full">
<div class="rounded main-box top content-full">
	<h3>Events</h3>
	<div class="form_errors"><?=validation_errors();?></div>
	<?php
		$attributes=array('name'=>'eventForm','class'=>'formClass');
		echo form_open(base_url().'admin/admin_events',$attributes);
		echo '<input type="hidden" name="eventActive" value="1" />';
		echo '<p style="margin-bottom:5px;">';
		echo form_label('Title: ','title');
		$formData = array(
			'name'=>'eventTitle',
			'id'=>'eventTitle',
			'value'=>set_value('eventTitle')
		);
		echo form_input($formData);
		echo '</p><p style="margin-bottom:5px;">';
		echo '</p><p class="textareabox" style="margin-bottom:5px;">';
		echo form_label('Body: ','body');
		$formData = array(
			'name'=>'eventData',
			'id'=>'body',
			'style'=>'height:125px;',
			'value'=>set_value('eventData')
		);
		echo form_textarea($formData);

		echo '</p><p style="margin-bottom:5px;">';
		echo form_label('Party Room: ','partyRoom');
		$array = array(0 => 'No', 1 => 'Yes');
		echo form_dropdown('eventPartyRoom',$array,'No','id="eventPartyRoom"');

		echo '</p><p style="margin-bottom:5px; display:none;" id="eventPublicDisplay">';
		echo form_label('Public Event','eventPublic');
		$array = array(0 => 'No', 1 => 'Yes');
		echo form_dropdown('eventPublic',$array,1,'id="eventPublic"');

		echo '</p><p style="margin-bottom:5px;">';
		echo form_label('Event Date: ','eventDate');
		echo '<input type="text" name="eventDate" id="eventDate" value="'.(set_value('eventDate') ? set_value('eventDate') : Date("Y-m-d")).'"/>';
		echo '</p><p style="margin-bottom:5px;">';


		echo '<p style="width:150px;margin-left:250px;">';
		$formData = array(
			'name'=>'submit',
			'id'=>'submit',
			'class'=>'button',
			'value'=>'Submit'
		);
		echo form_submit($formData);
		echo form_close();
	?>
</div>
<div class="rounded main-box content-full">
	<?php
		if(!isset($events))
		{
			echo 'There are no events at the moment';
		} else {
	?>
		<table class="tableClass small">
			<tr><th>Created By</th><th>Title</th><th>Body</th><th>Start</th><th>Actions</th></tr>
			<?php
				foreach($events as $event)
				{
					if($event['eventActive']==0)
					{
						$class = 'style="background-color:#DDD"';
					} else {
						$class='';
					}
					echo '<tr '.$class.'>';
					echo '<td>'.$event['building'].'-'.$event['unitNumber'].'</td>';
					echo '<td><input type="hidden" name="eventId" value="'.$event['eventId'].'"/>'.
						$event['eventTitle'].
						'</td><td>'.
						strip_tags(trim(substr($event['eventData'],0,50))).'...'.
						'</td><td align="center">'.
						date('M, d, Y',strtotime($event['eventDate']));

						echo '</td><td align="center">';
						echo '<input type="hidden" name="eventActive" id="eventActive" value="'.$event['eventActive'].'" />';
						echo '<input type="hidden" name="requestEmail" id="requestEmail" value="'.$event['requestEmail'].'" />';
						echo $event['eventActive'] == 1 ? '<a href="" id="activate" class="activate">Reject</a>' : '<a href="" id="activate" class="activate">Approve</a>';
						echo ' | <a href="'.base_url().'edit_events/'.$event['eventId'].'">Edit</a> | <a href="#" class="deleteMe">Delete</a>'.
						'</td><td></tr>';
				}
			?>
		</table>
	<?php
		}
	?>
</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".admin").addClass("active");
		$(".notifications").addClass("active");
		$(".admin_events").addClass("active");
		$( "#eventDate" ).datepicker({
				firstDay:0,
				dateFormat:"yy-m-d",
				constrainInput:true
			});

	$("#eventPartyRoom").on("change", function(){
		if($(this).val()==1)
		{
			$("#eventPublicDisplay").show();
		} else {
			$("#eventPublicDisplay").hide();
		}
	});

	$(".activate").on("click",function(){
		$row = $(this).closest('tr');
		$.ajax({
			type: "POST",
			url: "<?=base_url()?>admin/aj_event_display/",
			data:$row.find('input').serialize(),
			dataType: "json",
			success: function(content) {
				if (content.status == "success") {
					$("#eventActive", $row).val(content.message);
					if(content.message==1)
					{
						$(".activate",$row).html("Reject");
						$row.css("background-color","#FFF");
					} else {
						$(".activate",$row).html("Approve");
						$row.css("background-color","#DDD");
					}
					window.location.reload();
				} else {
					$("#error").html('<p>'+content.message+'</p>');
				}
			}
		});
		return false
	});

	$('.deleteMe').live("click", function(){
		$confirmDeletion = confirm("Are you sure you would like to delete this event?");
		if ($confirmDeletion == true)
		{
			$row = $(this).closest('tr');
			$.ajax({
				type: "POST",
				url: "<?=base_url()?>admin/aj_delete_event/",
				data: { eventId: $row.find('input[type=hidden][name=eventId]').val() },
				dataType: "json",
				success: function(content) {
					if (content.status == "success") {
						$row.hide();
					} else {
						$("#error").html('<p>'+content.message+'</p>');
					}
				}
			});
		}
		return false;
	});
	});
</script>
