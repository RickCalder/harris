<div class="content content-full">
<div class="rounded main-box top content-full">

	<h3>Editing event #<?=$eventId;?></h3>
	<div class="form_errors"><?=validation_errors();?></div>
	<?php
		$attributes=array('name'=>'eventForm','class'=>'formClass');
		echo form_open(base_url().'admin/edit_events',$attributes);
		echo '<input type="hidden" name="active" value="1" />';
		echo '<input type="hidden" name="createdBy" value="'.$this->session->userdata('userId').'" />';
		echo '<input type="hidden" name="eventId" value="'.$eventId.'" />';
		echo '<p style="margin-bottom:5px;">';
		echo form_label('Title: ','title');
		$formData = array(
			'name'=>'eventTitle',
			'id'=>'title',
			'value'=>$event[0]['eventTitle']
		);
		echo form_input($formData);
		echo '</p><p style="margin-bottom:5px;">';
		echo '</p><p class="textareabox" style="margin-bottom:5px;">';
		echo form_label('Body: ','body');
		$formData = array(
			'name'=>'eventData',
			'id'=>'body',
			'style'=>'height:125px;',
			'value'=>$event[0]['eventData']
		);

		echo form_textarea($formData);

		echo '</p><p style="margin-bottom:5px;">';
		echo form_label('Party Room: ','partyRoom');
		$array = array(0 => 'No', 1 => 'Yes');
		echo form_dropdown('eventPartyRoom',$array,$event[0]['eventPartyRoom'],'id="eventPartyRoom"');

		echo '</p><p style="margin-bottom:5px; display:none;" id="eventPublicDisplay">';
		echo form_label('Public Event','eventPublic');
		$array = array(0 => 'No', 1 => 'Yes');
		echo form_dropdown('eventPublic',$array,$event[0]['eventPublic'],'id="eventPublic"');

		echo '<p style="margin-bottom:5px;">';
		echo form_label('Date: ','eventDate');
		echo '<input type="text" name="eventDate" id="eventDate" value="'.($event[0]['eventDate'] ? date('Y-m-d',strtotime($event[0]['eventDate'])) : Date("Y-m-d")).'"/>';
		echo '</p><p style="margin-bottom:5px;">';
		echo '<p style="width:150px;margin-left:250px;">';
		$formData = array(
			'name'=>'submit',
			'id'=>'submit',
			'class'=>'button',
			'value'=>'Submit'
		);
		echo form_submit($formData);
		echo form_close();
	?>
</div>

</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".admin").addClass("active");
		$(".notifications").addClass("active");
		$(".admin_events").addClass("active");

		$( "#eventDate").datepicker({
				firstDay:0,
				dateFormat:"yy-m-d",
				constrainInput:true
			});

		if($("#eventPartyRoom").val()==1)
		{
			$("#eventPublicDisplay").show();
		} else {
			$("#eventPublicDisplay").hide();
		}
	$("#eventPartyRoom").on("change", function(){
		if($(this).val()==1)
		{
			$("#eventPublicDisplay").show();
		} else {
			$("#eventPublicDisplay").hide();
		}
	});
	});
</script>
