<div class="content content-full">
	<p class="top">Don't forget to add contact information!</p>
<div class="rounded main-box top content-full">
	<div class="right" style="width:400px;">
	<h3>Instructions</h3>

	<p class="small"><strong>Public - Yes means everyone can see the ad, No means only Owners can see the ad.</strong></p>
	<p class="small">The body of the ad is limited to 500 characters, that does include any HTML tags created by the editor buttons.</p>
	<p class="small"><strong>Email Links - </strong>To create an email link click the Link button above the editor and enter "mailto:" without the quotes before the email address. Example: mailto:john@doe.com After the link is created you can delete the mailto: from the editor window.</p>
	<p class="small"><strong>Web Links - </strong>To create an web link click the Link button above the editor and enter the website address. DO NOT delete the http:// To create a linked word, type the word first, highlight it then click the link button and enter the web address. The word will now link to the address you entered.</p>

	</div>
	<h3>Advertisements</h3>
	<div class="form_errors" id="error"><?=validation_errors();?></div>
	<?php //debug($advertisement);
		$attributes=array('name'=>'advertisements','class'=>'formClass');
		echo form_open(base_url().'admin/admin_advertisements',$attributes);
		echo '<input type="hidden" name="adActive" value="1" />';
		echo '<input type="hidden" name="createdBy" value="'.$this->session->userdata('userId').'" />';
		echo '<p style="margin-bottom:5px;">';
		echo form_label('Title: ','title');
		$formData = array(
			'name'=>'adTitle',
			'id'=>'adTitle',
			'value'=>set_value('adTitle')
		);
		echo form_input($formData);
		echo '</p><p style="margin-bottom:5px;">';

		echo '<p style="margin-bottom:5px;">';
		echo form_label('Ad Type: ','adType');
		echo form_dropdown('adTypeId',$adTypes,set_value('adTypeId'),'id="adTypeId"');
		echo '</p><p style="margin-bottom:5px;">';

		echo '<p style="margin-bottom:5px;">';
		echo form_label('Public: ','adPublic');
		$array = array(0=>'No',1=>'Yes');
		echo form_dropdown('adPublic',$array,set_value('adPublic'),'id="adPublic"');
		echo '</p><p style="margin-bottom:5px;">';

		echo '</p><p class="textareabox" style="margin-bottom:5px;">';
		echo form_label('Body: ','body');
		$formData = array(
			'name'=>'adBody',
			'id'=>'adBody',
			'style'=>'height:125px;',
			'value'=>set_value('adBody')
			);
		echo form_textarea($formData);
		echo '</p><p style="width:150px;margin-left:250px;">';
		$formData = array(
			'name'=>'submit',
			'id'=>'submit',
			'class'=>'button',
			'value'=>'Submit'
		);
		echo form_submit($formData);
		echo form_close();
	?>

</div>
<div class="rounded main-box content-full">
	<?php
		if(!$advertisements)
		{
			echo 'There are no Advertisements at the moment';
		} else {
	?>
		<table class="tableClass small">
			<tr><th>Title</th><th>Body</th><th>Expires</th><th>Actions</th></tr>
			<?php
				foreach($advertisements as $ad)
				{
					if($ad['display']==0)
					{
						$class = 'style="background-color:#DDD"';
					} else {
						$class='';
					}
					echo '<tr '. $class.'><td><input type="hidden" name="adId" value="'.$ad['adId'].'"/>'.
						$ad['adTitle'].
						'</td><td>'.
						strip_tags(trim(substr($ad['adBody'],0,50))).'...'.
						'</td><td align="center">'.
						date('M, d, Y',strtotime($ad['adExpires'])).
						'</td><td align="center"><a href="'.base_url().'edit_advertisement/'.$ad['adId'].'">Edit</a> | <a href="#" class="deleteMe">Delete</a>&nbsp;|&nbsp;';
						echo '<input type="hidden" name="display" id="display" value="'.$ad['display'].'" />';
						echo $ad['display'] == 1 ? '<a href="" id="activate" class="activate">Hide</a>' : '<a href="" id="activate" class="activate">Display</a>';
						echo '</td></tr>';
				}
			?>
		</table>
	<?php
		}
	?>
</div>
</div>
<script type="text/javascript">

	$(".activate").on("click",function(){
		$row = $(this).closest('tr');
		$.ajax({
			type: "POST",
			url: "<?=base_url()?>admin/aj_ad_display/",
			data:$row.find('input').serialize(),
			dataType: "json",
			success: function(content) {
				if (content.status == "success") {
					$("#display", $row).val(content.message);
					if(content.message==1)
					{
						$(".activate",$row).html("Hide");
						$row.css("background-color","#FFF");
					} else {
						$(".activate",$row).html("Display");
						$row.css("background-color","#DDD");
					}
				} else {
					$("#error").html('<p>'+content.message+'</p>');
				}
			}
		});
		return false
	});

	$('.deleteMe').live("click", function(){
		$confirmDeletion = confirm("Are you sure you would like to delete this advertisement?");
		if ($confirmDeletion == true)
		{
			$row = $(this).closest('tr');
			$.ajax({
				type: "POST",
				url: "<?=base_url()?>admin/aj_delete_ad/",
				data: { adId: $row.find('input[type=hidden][name=adId]').val() },
				dataType: "json",
				success: function(content) {
					if (content.status == "success") {
						$row.hide();
					} else {
						$("#error").html('<p>'+content.message+'</p>');
					}
				}
			});
		}
		return false;
	});

	$("#submit").on("click",function(){
		var errors = '';
		if ($("#adTitle").val()=='')
			errors += '<li>You must enter a Title!</li>';
		if ($("#adBody").val()=='')
			errors += '<li>You must enter a Body!</li>';
		if ($("#adTypeId").val()==0)
			errors += '<li>You must select a type of Ad!</li>';
		if(errors)
		{
			$("#error").html('<ul>' + errors + '</ul>');
			return false;
		} else {
			if($("#adPublic").val()==1)
				if(!confirm("Are you sure you want this ad displayed to the general public?"))
				{
					return false;
				} else {
					return true;
				}
		}
	});

	$(document).ready(function(){
		$(".admin").addClass("active");
		$(".notifications").addClass("active");
		$(".admin_advertisements").addClass("active");
            $("#adBody").htmlarea({
                // Override/Specify the Toolbar buttons to show
                toolbar: [
                    ["bold", "italic", "underline"],
                    ["link", "unlink"]
                ],

                // Do something once the editor has finished loading
                loaded: function() {
                    //// 'this' is equal to the jHtmlArea object
                    //alert("jHtmlArea has loaded!");
                    //this.showHTMLView(); // show the HTML view once the editor has finished loading
                }
            });
        });

</script>
