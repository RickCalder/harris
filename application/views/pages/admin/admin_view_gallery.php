<div class="content content-full">
<? if($gallery[0]['galleryId']) { ?>
<h2 class="top">Gallery: <?=$gallery[0]['galleryName']?></h2>
<p>You can set images not to display by unchecking the box on the individual image and choosing update. You can also add a caption to the image that will be displayed in the Owners gallery view.<br /><a href="<?=base_url();?>admin_image_gallery">Return to galleries view</a></p>

<?php
	echo form_open('imageUpdate');
	foreach($gallery as $image)
	{
		echo '<div class="thumbcontainer">
			<input type="hidden" name="galleryImageId" value="'.$image['galleryImageId'].'"/>';
		echo '<div class="thumbs" id="'.$image['galleryImageId'].'">';
		echo '<input type="checkbox" name="imageActive" id="imageActive" '.($image['imageActive'] ==1 ? 'checked' : '').' />';
		echo '<input type="hidden" name="galleryImageId" value="'.$image['galleryImageId'].'"/>';
		echo '<a href="'.base_url().'assets/images/galleries/'.$image['galleryFolderName'].'/lrg_'.$image['image'].'"rel="prettyPhoto">
				<img src="'.base_url().'assets/images/galleries/'.$image['galleryFolderName'].'/med_'.$image['image'].'" /></a><br />';
		echo '<input type="text" name="imageCaption" placeholder="caption" value="'.$image['imageCaption'].'"></input><br />';
		echo '<a href="" class="update">Update</a>';
		echo '&nbsp;|&nbsp;<a href="" class="deleteLink">Delete</a>';
		echo '</div>';
		echo '</div>';
	}
	echo form_close();
?>

<? }else {
	echo '<h2 class="top">Gallery Not Found</h2>
	         <a href="'.base_url().'admin_image_gallery">Return to gallery listings</a>';
	}
?>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".admin").addClass("active");
		$(".adminImageGallery").addClass("active");
	});
	
	$(".update").on("click",function(){
		$(".thumbs").each(function(){
			$(this).css("background-color","#FFF");
		});
		$row = $(this).closest("div");
		$.ajax({
			type: "GET",
			url: "<?=base_url()?>admin/ajUpdateGallery",
			data: $row.find('input').serialize(),
			dataType: "json",
			success: function(content) {
				if (content.status == "success") {
					$row.stop().css("background-color","#99FF66");
				}
			}
		});
		return false;
	});
	
	$(".deleteLink").on("click",function(){
		if(!confirm('Are you sure you want to delete this image?'))
		return false;
		$row = $(this).closest('div');
		$.ajax({
			type: "POST",
			url: "<?=base_url()?>admin/aj_imageDelete",
			data: $row.find('input').serialize(),
			dataType: "json",
			success: function(content) {
				window.location="<?=base_url();?>admin_view_gallery/" + <?=$gallery[0]['galleryId']?>;
			}
		});
		return false;
	});
</script>
