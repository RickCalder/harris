<div class="content content-full">
<div class="rounded main-box top content-full">
	<h3>Contractor Types</h3>
	<div class="form_errors" id="error"><?=validation_errors();?></div>
	<?php //debug($advertisement);
		$attributes=array('name'=>'contractors','class'=>'formClass');
		echo form_open(base_url().'admin/add_contractor_type',$attributes);
		echo '<input type="hidden" name="contractorActive" value="1" />';
		echo '<p style="margin-bottom:5px;">';
		echo form_label('Type: ','name');
		$formData = array(
			'name'=>'contractorTypeName',
			'id'=>'contractorTypeName',
			'value'=>set_value('contractorTypeName')
		);
		echo form_input($formData);
		echo '</p><p style="margin-bottom:5px;">';
		echo '</p><p style="width:150px;margin-left:250px;">';
		$formData = array(
			'name'=>'submit',
			'id'=>'submit',
			'class'=>'button',
			'value'=>'Submit'
		);
		echo form_submit($formData);
		echo form_close();
	?>

</div>
<div class="rounded main-box content-full">
	<?php
		if(!$contractorTypes)
		{
			echo 'There are no Contractors at the moment';
		} else {
	?>
		<table class="tableClass small">
			<tr><th>Type</th><th>Actions</th></tr>
			<?php
				foreach($contractorTypes as $type)
				{

					if($type->contractorTypeActive == 0)
					{
						$class = ' style="background-color:#DDD"';
					} else {
						$class='';
					}
					echo '<tr'.$class.'>
						<td>'.$type->contractorTypeName.'
						<input type="hidden" name="contractorTypeActive" value="'.$type->contractorTypeActive.'" id="contractorActive" />
						<input type="hidden" name="contractorTypeId" value="'.$type->contractorTypeId.'" />
						</td>
						<td align="center"><a href="#" class="deleteMe">Delete</a>&nbsp;</td></tr>';
				}
			?>
		</table>
	<?php
		}
	?>
</div>
</div>
<script type="text/javascript">
	$('.deleteMe').live("click", function(){
		$confirmDeletion = confirm("Are you sure you would like to delete this contractor type?");
		if ($confirmDeletion == true)
		{
			$row = $(this).closest('tr');
			$.ajax({
				type: "POST",
				url: "<?=base_url()?>admin/aj_delete_contractor_type/",
				data: { contractorTypeId: $row.find('input[type=hidden][name=contractorTypeId]').val() },
				dataType: "json",
				success: function(content) {
					if (content.status == "success") {
						$row.hide();
					} else {
						$("#error").html('<p>'+content.message+'</p>');
					}
				}
			});
		}
		return false;
	});


	$(document).ready(function(){
		$(".admin").addClass("active");
		$(".notifications").addClass("active");
		$(".add_contractor_type").addClass("active");

        });
</script>
