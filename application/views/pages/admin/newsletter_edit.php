<div class="content">
	<div id="errors" class="form_errors">
	</div>
	<p>
	<?php 
		$attributes=array('id'=>'newsletterForm');
		echo form_open('admin/newsletter_edit',$attributes);
		echo form_label('Newsletter Title: ','newsletterHeading');
		if(isset($newsletter))
		{
			$newsletterTitle= $newsletter[0]['newsletterHeading'];
			$newsletterId = $newsletter[0]['newsletterId'];
			$newsletterBody = $newsletter[0]['newsletterBody'];
			echo '<script> var nType = "' . $newsletter[0]['type'] . '"</script>';
		} else {
			$newsletterTitle='';
			$newsletterId='';
			$newsletterBody='';
			echo '<script> var nType = "no"</script>';
		}
		$formdata=array(
			'name'=>'newsletterHeading',
			'id'=>'newsletterHeading',
			'placeholder'=>'Winter 2013',
			'style'=>'width:300px;',
			'value'=>$newsletterTitle
		);
		echo form_input($formdata);
		echo form_hidden('newsletterId',$newsletterId);
	?>
	</p>
	<p>
		<label for="type-select">Type of document</label>
		<select id="type-select" name="type">
			<option value="no">Please select a type</option>
			<option value="newsletter">Newsletter</option>
			<option value="pi">Periodic Information Certificate</option>
		</select>
	</p>
	<textarea cols="80" id="newsletterBody" name="newsletterBody" rows="60">
		<?php
				echo $newsletterBody;
		?>
	</textarea>
	<?php
		$formdata=array(
			'name'=>'submit',
			'class'=>'button',
			'value'=>'Save Newsletter'
		);
		echo form_submit($formdata);
		echo form_close();
	?>
</div>
<script type="text/javascript">
	CKEDITOR.replace( 'newsletterBody' );
	
	$(document).ready(function(){
		$(".admin").addClass("active");
		$(".newsletter_create").addClass("active");
		$(".newslettersmenuadmin").addClass("active");
		$("#type-select").val(nType)
	});

	$("#type-select").on("change", function() {
		if($(this).val() == 'pi') {
			var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
			var d = new Date();
			var date = months[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear()
			$("#newsletterHeading").val("PIC - " + date)
		} else {
			$("#newsletterHeading").val('')
		}
	})
	
	$("#newsletterForm").submit(function(){
		var errors ="";
		if($("#newsletterHeading").val()=="" || $("#newsletterBody").val()=="")
		{
			errors += "* You must enter a title and newsletter content to continue!";
		} else if( $("#type-select").val() == 'no') {
			errors += "You must select a type of document to create"
		}
		if(errors)
		{
			$("#errors").html(errors);
			return false;
		} 
	});
</script>
