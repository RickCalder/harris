<div class="content content-full">
	<div class="right">
		<?php
			if(isset($newsletters[0]))
			{
				$newsId = $newsletters[0]['newsletterId'];
			} else {
				$newsId = 0;
			}
			echo form_label('Choose a PIC: ','newslist');
			$attributes = 'id="newslist" style="width:200px;"';
			echo form_dropdown('newslist',$newslettersDrop,$newsId,$attributes);
		?>
	</div>
	<?php
		if(isset($newsletters[0]))
		{
	?>
		<h2 class="top"><?=$newsletters[0]['newsletterHeading'];?></h2>
		<div class="main-box rounded content-full">
			<?=$newsletters[0]['newsletterBody'];?>
		</div>
	<?php
		} else {
	?>		
		<h2 class="top">Periodic Information Certificate not found</h2>
		<div class="main-box rounded content-full">
			<p>We're sorry, the periodic information certificate you're looking for does not exist. Please select a different periodic information certificate from the list.</p>
		</div>
	<?php
		}
	?>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".periodics").addClass("active");
		$(".periodicsmenu").addClass("active");
		$(".owners").addClass("active");
	});
	$("#newslist").on("change",function(){
		if($("#newslist").val()==9999999)
		{
			window.location='<?=base_url();?>'+"periodics_archive";
		} else {
			window.location='<?=base_url();?>'+"periodics/"+$("#newslist").val();
		}
	});
</script>
