<div class="content content-full">
<h2 class="top">Announcements</h2>
<?php
	foreach($announcements as $announcement)
	{
?>
		<div class="announceDiv">
			<h4><?=$announcement['announcementTitle'];?></h4>
			<p><?=$announcement['announcementBody'];?></p>
		</div>
<?php
	}
?>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".owners").addClass("active");
		$(".whats_happening").addClass("active");
		$(".owners_announcements").addClass("active");
	});
</script>
