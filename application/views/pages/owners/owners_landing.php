
<?php
  $postLink = (in_array('basic_admin',$this->session->userdata('roleNames'))? base_url()."admin_advertisements" : base_url()."post_ad" );
?>

<div class="content content-full">
<h2 class="top">Owner Home Page</h2>

<!--Classified Ads-->
<h3>
  Classified Ads 
  <small><a href="<?=$postLink;?>">Post an ad</a></small>
</h3>

<?php
  if($advertisements)
  {

  foreach($advertisements as $ads)
  {
?>
    <div class="owner-separator-div">
      <h4 class="top"><?=$ads['adType'];?></h4>
      <strong><?=$ads['adTitle'];?></strong>
      <p><?=$ads['adBody'];?></p>
    </div>
<?php
  }
} else {
  echo '<p class="top">There are no classified advertisements at the moment. Check back later!</p>';
}
?>
<hr />
<!--Announcements-->
<h3 class="top">Announcements</h3>
<?php
  foreach($announcements as $announcement)
  {
?>
    <div class="owner-separator-div">
      <h4><?php echo $announcement['announcementTitle'];?></h4>
      <p><?php echo $announcement['announcementBody'];?></p>
    </div>
<?php
  }
?>
</div>
<hr />
<!--Newsletters-->
<h3>Most recent newsletters</h3>
<table class="tableClass">
<?php
  $x=0;
	foreach($newsletters as $newsletter)
	{
    if($x <=5){
  		echo '<tr><td>'.$newsletter['newsletterHeading'].'</td><td>'.date('M, d, Y',strtotime($newsletter['creationDateTime'])).'</td>'.
  		'<td><a href="'.base_url().'newsletters/'.$newsletter['newsletterId'].'">View</a></td>'.
  		'</td></tr>';
    }
    $x++;
	}
?>
</table>
<p><a href="/newsletters_archive">View All Newsletters</a></p>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".owners").addClass("active");
	});
</script>
