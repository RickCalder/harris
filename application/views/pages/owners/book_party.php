<div class="content content-full">
<div class="rounded main-box top content-full">

	<h3>Book an event in the party room</h3>
	<div class="form_errors" id="error"><?=validation_errors();?></div>
	<?php
		$attributes=array('name'=>'book_party','class'=>'formClass');
		echo form_open(base_url().'book_party',$attributes);
		echo '<input type="hidden" name="eventActive" value="0" />';
		echo '<input type="hidden" name="eventPartyRoom" value="1" />';
		echo '<input type="hidden" name="createdBy" value="'.$this->session->userdata('userId').'" />';
		echo '<p style="margin-bottom:5px;">';
		echo form_label('Event: ','eventTitle');
		$formData = array(
			'name'=>'eventTitle',
			'id'=>'eventTitle',
			'value'=>set_value('eventTitle')
		);
		echo form_input($formData);
		echo '</p><p style="margin-bottom:5px;">';

		echo '<p style="margin-bottom:5px;">';
		echo form_label('Description: ','eventData');
		$formData = array(
			'name'=>'eventData',
			'id'=>'eventData',
			'value'=>set_value('eventData')
		);
		echo form_input($formData);
		echo '</p><p style="margin-bottom:5px;">';

		echo '<p style="margin-bottom:5px;">';
		echo form_label('Public: ','adPublic');
		$array = array(0=>'No',1=>'Yes');
		echo form_dropdown('eventPublic',$array,set_value('eventPublic'),'id="eventPublic"');
		echo '</p><p style="margin-bottom:5px;">';

		echo '</p><p style="margin-bottom:5px;">';
		echo form_label('Date: ','eventDate');
		$formData = array(
			'name'=>'eventDate',
			'id'=>'eventDate',
			'value'=>set_value('eventDate')
			);
		echo form_input($formData);
		echo '<p class="event_date"></p>';
		echo form_label('Email: ','eventData');
		$userEmail = set_value('requestEmail') ? set_value('requestEmail') : $user[0]['emailAddress'];
		$formData = array(
			'name'=>'requestEmail',
			'id'=>'requestEmail',
			'value'=>$userEmail
		);
		echo form_input($formData);
		echo '</p><p style="margin-bottom:5px;">';
		echo '</p><p style="width:150px;margin-left:250px;">';
		$formData = array(
			'name'=>'submit',
			'id'=>'submit',
			'class'=>'button',
			'value'=>'Submit'
		);
		echo form_submit($formData);
		echo form_close();
	?>
	<p>Please note that if your request is approved, you must submit 2 cheques to the Superintendent<br />($50.00 and $150.00) made out to WCC 76.</p>
	<div class="clear"></div>

</div>
</div>
<script type="text/javascript">

	$(document).ready(function(){
		$(".owners").addClass("active");
		$(".notifications").addClass("active");
		$(".admin_advertisements").addClass("active");
		$( "#eventDate").datepicker({
						firstDay:0,
						dateFormat:"yy-m-d",
						constrainInput:true,
						minDate:0,
			onSelect: function(){
				$.ajax({
				type: "POST",
				url: "<?=base_url()?>owners/aj_check_date/",
				data: { eventDate: $(this).val() },
				dataType: "json",
				success: function(content) {
					if (content.status == "success") {
						$(".event_date").css('color',content.cssClass);
						$(".event_date").html(content.message);
					}
				}
			});
			}
		});
		$("#submit").on("click",function(){
			var errors;
			if($("#eventTitle").val()=='')
			{
				errors = '<li>Event is a required field</li>';
			}
			if($("#eventData").val()=='')
			{
				errors += '<li>Description is a required field</li>';
			}
			if($("#eventDate").val()=='')
			{
				errors += '<li>Date is a required field</li>';
			}
			if($("#requestEmail").val()=='')
			{
				errors += '<li>Email is a required field</li>';
			}
			if(errors)
			{
				$("#error").html(errors);
				return false;
			}
		});

    });

</script>
