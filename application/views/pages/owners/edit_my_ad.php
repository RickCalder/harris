<div class="content content-full">
	<p class="top">Don't forget to add your contact information!</p>
<div class="rounded main-box top content-full">
	<div class="right" style="width:400px;">
	<h3>Instructions</h3>
	<p class="small"><strong>Public - Yes means everyone can see the ad, No means only Owners can see the ad.</strong></p>
	<p class="small">The body of the ad is limited to 500 characters, that does include any HTML tags created by the editor buttons.</p>
	<p class="small"><strong>Email Links - </strong>To create an email link click the Link button above the editor and enter "mailto:" without the quotes before the email address. Example: mailto:john@doe.com After the link is created you can delete the mailto: from the editor window.</p>
	<p class="small"><strong>Web Links - </strong>To create an web link click the Link button above the editor and enter the website address. DO NOT delete the http:// To create a linked word, type the word first, highlight it then click the link button and enter the web address. The word will now link to the address you entered.</p>

	</div>
	<h3>Advertisements</h3>
	<div class="form_errors" id="error"><?=validation_errors();?></div>
	<?php //debug($advertisement);
		$attributes=array('name'=>'advertisements','class'=>'formClass');
		echo form_open(base_url().'owners/edit_ad',$attributes);
		echo '<input type="hidden" name="adActive" value="1" />';
		echo '<input type="hidden" name="adId" value="'.$advertisement[0]['adId'].'" />';
		echo '<input type="hidden" name="createdBy" value="'.$this->session->userdata('userId').'" />';
		echo '<p style="margin-bottom:5px;">';
		echo form_label('Title: ','title');
		$formData = array(
			'name'=>'adTitle',
			'id'=>'adTitle',
			'value'=>(set_value('adTitle') ? set_value('adTitle') : $advertisement[0]['adTitle'])
		);
		echo form_input($formData);
		echo '</p><p style="margin-bottom:5px;">';

		echo '<p style="margin-bottom:5px;">';
		echo form_label('Ad Type: ','adType');
		echo form_dropdown('adTypeId',$adTypes,(set_value('adTypeId') ? set_value('adTypeId') : $advertisement[0]['adTypeId']),'id="adTypeId"');
		echo '</p><p style="margin-bottom:5px;">';

		echo '<p style="margin-bottom:5px;">';
		echo form_label('Public: ','adPublic');
		$array = array(0=>'No',1=>'Yes');
		echo form_dropdown('adPublic',$array,(set_value('adPublic') ? set_value('adPublic') : $advertisement[0]['adPublic']),'id="adPublic"');
		echo '</p><p style="margin-bottom:5px;">';

		echo '</p><p class="textareabox" style="margin-bottom:5px;">';
		echo form_label('Body: ','body');
		$formData = array(
			'name'=>'adBody',
			'id'=>'adBody',
			'style'=>'height:125px;',
			'value'=>(set_value('adBody') ? set_value('adBody') : $advertisement[0]['adBody'])
		);
		echo form_textarea($formData);
		echo '</p><p style="width:150px;margin-left:250px;">';
		$formData = array(
			'name'=>'submit',
			'id'=>'submit',
			'class'=>'button',
			'value'=>'Submit'
		);
		echo form_submit($formData);
		echo form_close();
	?>

</div>
</div>
<script type="text/javascript">

	$("#submit").on("click",function(){
		var errors = '';
		if ($("#adTitle").val()=='')
			errors += '<li>You must enter a Title!</li>';
		if ($("#adBody").val()=='')
			errors += '<li>You must enter a Body!</li>';
		if ($("#adTypeId").val()==0)
			errors += '<li>You must select a type of Ad!</li>';
		if($("#adBody").val().length > 500)
			errors += '<li>You have more than the maximum of 500 characters in the Body</li>';
		if(errors)
		{
			$("#error").html('<ul>' + errors + '</ul>');
			return false;
		} else {
			if($("#adPublic").val()==1)
				if(!confirm("Are you sure you want this ad displayed to the general public?"))
				{
					return false;
				} else {
					return true;
				}
		}
	});

	$(document).ready(function(){
		$(".admin").addClass("active");
		$(".notifications").addClass("active");
		$(".admin_advertisements").addClass("active");
            $("#adBody").htmlarea({
                // Override/Specify the Toolbar buttons to show
                toolbar: [
                    ["bold", "italic", "underline"],
                    ["link", "unlink"]
                ],

                // Do something once the editor has finished loading
                loaded: function() {
                    //// 'this' is equal to the jHtmlArea object
                    //alert("jHtmlArea has loaded!");
                    //this.showHTMLView(); // show the HTML view once the editor has finished loading
                }
            });
        });

</script>
