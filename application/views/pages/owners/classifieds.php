<div class="content content-full">
<h2 class="top">Classified Ads</h2>
<?php
	$postLink = (in_array('basic_admin',$this->session->userdata('roleNames'))? base_url()."admin_advertisements" : base_url()."post_ad" );
?>
<a href="<?=$postLink;?>"><h4>Post an ad</h4></a>
<?php
	if($advertisements)
	{

	foreach($advertisements as $ads)
	{
?>
		<div class="announceDiv">
			<h4 class="top"><?=$ads['adType'];?></h4>
			<strong><?=$ads['adTitle'];?></strong>
			<p><?=$ads['adBody'];?></p>
		</div>
<?php
	}
} else {
	echo '<h3 class="top">There are no classified advertisements at the moment. Check back later!</h3>';
}
?>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".owners").addClass("active");
		$(".whats_happening").addClass("active");
		$(".classifieds").addClass("active");
	});

	$("#filter").on("change",function(){
		alert($(this).val());
	})
</script>
