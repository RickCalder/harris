<div class="content content-full">
<div class="right"><p class="top"><a href="<?=base_url();?>book_party">Book the party room</a></p></div>
<h2 class="top">Event Calendar</h2>
<?=$calendar;?>
<div id="cal_info" class="rounded"></div>
</div>
<script type="text/javascript">
	var message;
	$(document).ready(function(){
		$(".owners").addClass("active");
		$(".owners_calendar").addClass("active");
		$(".whats_happening").addClass("active");
	});

	$(".event").on("click",function(){
		$.when(getData($(this).attr("id"))).done(function(){
			$("#cal_info").html(message);
		});
		return false;
	});
	function getData(id)
	{
		$.ajax({
			type: "GET",
			url: "<?=base_url()?>owners/ajGetEvent",
			data: {eventId: id},
			dataType: "json",
			async:false,
			success: function(content) {
				if (content.status == "success") {
					message ="<p>"+content.message+"</p>";
				}
			}
		});
		return false;
	}
	$(".cal_content").each(function(){
		var partyRoom = 0;
		$("li", this).each(function(){
			if($(this).text().toLowerCase().indexOf("party room") > -1)
			{
				partyRoom +=1;
			}
			if (partyRoom > 0)
			{
				$(this).closest("td").css('background-color','#FF9999');
			} else {
				$(this).closest("td").css('background-color','#CCFF66');
			}

		});
	});

	$(".notDay").each(function(){
		$(this).parent().css("background-color","#FFF");
	});
</script>
