<div class="content">

<h2 class="top">Periodic Information Certificates</h2>

<table class="tableClass">

<?php

	foreach($newsletters as $newsletter)

	{

		echo '<tr><td>'.$newsletter['newsletterHeading'].'</td><td>'.date('M, d, Y',strtotime($newsletter['creationDateTime'])).'</td>'.

		'<td><a href="'.base_url().'periodics/'.$newsletter['newsletterId'].'">View</a></td>'.

		'</td></tr>';

	}

?>

</table>
<?php 

	if(count($newsletters) ===0) {
		echo "<p>There are no current Periodic Information Certificates. Check back later.</p>";
	}
	 ?>

</div>

<script type="text/javascript">

	$(document).ready(function(){

		$(".owners").addClass("active");

		$(".periodicsmenu").addClass("active");

		$(".periodics_archive").addClass("active");

	});

</script>

