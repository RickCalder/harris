<div class="content content-full">
<? if($gallery[0]['galleryId']) { ?>
<h2 class="top">Gallery: <?=$gallery[0]['galleryName']?></h2>
<p><a href="<?=base_url();?>galleries">Back to gallery list.</a><br />Click a photo to see a full size view.</p>

<?php
	echo form_open('imageUpdate');
	foreach($gallery as $image)
	{
		echo '<div class="thumbcontainer">';
		echo '<div class="thumbs">';
		echo '<a href="'.base_url().'assets/images/galleries/'.$image['galleryFolderName'].'/lrg_'.$image['image'].'"rel="prettyPhoto">
				<img src="'.base_url().'assets/images/galleries/'.$image['galleryFolderName'].'/med_'.$image['image'].'" /></a><br />';
		echo '<p class="small">'.$image['imageCaption'].'</p>';
		echo '</div>';
		echo '</div>';
	}
	echo form_close();
?>

<? }else {
	echo '<h2 class="top">Gallery Not Found</h2>
	         <a href="'.base_url().'admin_image_gallery">Return to gallery listings</a>';
	}
?>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".owners").addClass("active");
		$(".owners_gallery").addClass("active");
	});
	
</script>
