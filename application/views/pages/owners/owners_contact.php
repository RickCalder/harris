<div class="content content-full">
<h2 class="top">Contacts</h2>
<div class="main-box rounded content-full">
	<h3 class="first">Board Members</h3>
	<table class="tableClass">
		<tr><th>Name</th><th>Position</th><th>Phone</th><th>Email</th><th>Unit</th></tr>
	<?php
		if(isset($board))
		{
			foreach($board as $member)
			{
				echo '<tr>';
				echo '<td>'.$member['boardFirstName'].' '.$member['boardLastName'].'</td>';
				echo '<td>'.$member['boardTitle'].'</td>';
				echo '<td align="center">'.$member['boardPhone'].'</td>';
				echo '<td align="center"><a href="mailto:'.$member['boardEmail'].'">'.$member['boardEmail'].'</a></td>';
				echo '<td align="center">'.$member['boardUnitNumber'].'</td>';
				echo '<tr>';
			}
		}
	?>
	</table>
</div>
	<h3 class="top">Superintendant</h3>
	<div class="main-box rounded content-full">
		<p class="top">Doug Corbett<br />108-30 Harrisford Street<br />Hamilton, ON&nbsp;&nbsp;L8K 6N1<p>
		<p><strong>Telephone:</strong> <a href="tel:9055605536">905-560-5536</a>
		<p><strong>Email:</strong>  <a href="mailto:harristowers@rogers.com">harristowers@rogers.com</a></p>
	</div>
	<h3 class="first">Property Management</h3>
	<div class="main-box rounded content-full">
		<p class="top">Pat Kummer<br />Property Manager<br />Precision Management Services Inc.<br />33 King Street East, Suite 9<br />Dundas, ON&nbsp;&nbsp;L9H 1B7<p>
		<p><strong>Telephone:</strong> <a href="tel:905-544-0077">905-544-0077</a></p>
		<p><strong>Fax:</strong> 905-627-3530</p>
		<p><strong>Email:</strong> <a href="mailto:precision@cogeco.net">precision@cogeco.net</a></p>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".owners").addClass("active");
		$(".owners_contact").addClass("active");
	});
</script>
