<div class="content">
<h2 class="top">Newsletters</h2>
<table class="tableClass">
<?php
	foreach($newsletters as $newsletter)
	{
		echo '<tr><td>'.$newsletter['newsletterHeading'].'</td><td>'.date('M, d, Y',strtotime($newsletter['creationDateTime'])).'</td>'.
		'<td><a href="'.base_url().'newsletters/'.$newsletter['newsletterId'].'">View</a></td>'.
		'</td></tr>';
	}
?>
</table>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".owners").addClass("active");
		$(".newslettersmenu").addClass("active");
		$(".newsletters_archive").addClass("active");
	});
</script>
