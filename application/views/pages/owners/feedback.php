<div class="content content-full">
<h2 class="top">Website Feedback</h2>
<div class="main-box rounded content-full">
<p class="first">
	Please use the form below to provide any feedback, questions or suggestions you may have about this website. Remember this website is your website and we strive to ensure it's easy to use and provides you with all the information you may need.
</p>
<p style="margin-bottom:20px;">
	All fields are required.
</p>

<?php 
	$attributes=array('name'=>'feedbackForm','class'=>'formClass');
	echo form_open(base_url().'owners/feedback_form',$attributes);
	echo '<p style="margin-bottom:5px;">';
	echo form_label('Name: ','name');
	$formData = array(
		'name'=>'name',
		'placeholder'=>'John Doe',
		'id'=>'name'
	);
	echo form_input($formData);
	echo '<span class="nameError" style="color:#FF0000; margin-left:15px;"></span>';
	echo '</p><p style="margin-bottom:5px;">';
	echo form_label('Email: ','email');
	$formData = array(
		'name'=>'email',
		'placeholder'=>'john.doe@acme.com',
		'id'=>'email'
	);
	echo form_input($formData);
	echo '<span class="emailError" style="color:#FF0000; margin-left:15px;"></span>';
	echo '</p><p style="margin-bottom:5px;">';
	echo form_label('Subject: ','subject');
	$formData = array(
		'name'=>'subject',
		'id'=>'subject'
	);
	echo form_input($formData);
	echo '<span class="subjectError" style="color:#FF0000; margin-left:15px;"></span>';
	echo '</p><p class="textareabox" style="margin-bottom:5px;">';
	echo form_label('Comments: ','comments');
	$formData = array(
		'name'=>'comments',
		'id'=>'comments'
	);
	echo form_textarea($formData);	
	echo '<span class="commentsError" style="color:#FF0000; margin-left:15px;"></span>';
	echo '</p><p style="width:150px;margin-left:250px;">';
	$formData = array(
		'name'=>'submit',
		'id'=>'submit',
		'class'=>'button',
		'value'=>'Submit'
	);
	echo form_submit($formData);
	echo form_close();
	echo '</p>';
?>
</div>

</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".owners").addClass("active");
		$(".feedback").addClass("active");
		$("#name").focus();
	});
	$("#submit").on("click", function(){
	var email = $("#email").val();
	var errors = 0
		if($("#name").val()=="")
		{
			$(".nameError").html("* Name is a required field!");
			errors++;
		} else {
			$(".nameError").html("");
		}
		if($("#email").val()=="")
		{
			$(".emailError").html("* Email is a required field!");
			errors++;
		} else {
			$(".emailError").html("");
		}
		if( !isValidEmailAddress( email ) )
		{
			$(".emailError").html("* Please enter a valid email address!");
			errors++;
		} else {
			$(".emailError").html("");
		}
		if($("#subject").val()=="")
		{
			$(".subjectError").html("* Subject is a required field!");
			errors++;
		} else {
			$(".subjectError").html("");
		}
		if($("#comments").val()=="")
		{
			$(".commentsError").html("* Comments is a required field!");
			errors++;
		} else {
			$(".commentsError").html("");
		}
		if(errors>0)
		{
			return false;
		}
	});
	
function isValidEmailAddress(email) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(email);
};
</script>
