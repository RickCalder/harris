<div class="content content-full">
<h2 class="top">Approved Contractors</h2>
<p>Approved contractors are businesses that have worked in our buildings in the past and have shown themselves to be conscientious and professional. You are not required to hire a contractor from this list, they are simply suggestions.</p>
	<p>
		<?php if($contractorTypes){;?>
		<label for="contractorTypes">Filter by:</label>
		<?php
		$attributes=array('name'=>'approved_contractors','class'=>'formClass');
		echo form_open(base_url().'approved_contractors',$attributes);
		echo form_dropdown('contractorType',$contractorTypes);
		$formdata = array(
			'name'=>'submit',
			'value'=>'Go',
			'class'=>'button'
			);
		echo form_submit($formdata);
		echo form_close();
	}
		?>
	</p>
<div class="main-box rounded content-full">

	<?php
		if(isset($contractors) && $contractors)
		{
	;?>
	<table class="tableClass">
		<tr><th>Name</th><th>Type</th><th>Description</th><th>Phone</th></tr>
		<?php
			foreach($contractors as $contractor)
			{
				echo '<tr>';
				echo '<td>'.$contractor->contractorName.'</td>';
				echo '<td>'.$contractor->contractorTypeName.'</td>';
				echo '<td width="375">'.$contractor->contractorDescription.'</td>';
				echo '<td align="center">'.$contractor->contractorPhone.'</td>';
				echo '<tr>';
			}
		?>

	</table>
	<?php
		} else {
	?>
	<p>There are no approved contractors on the list, please check back soon as we add contractors!</p>
	<?php
	}
	?>
</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".owners").addClass("active");
		$(".approved_contractors").addClass("active");
	});
</script>
