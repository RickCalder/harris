<div class="content">
	<h2 class="top">Wentworth Condominium Corporation No. 76</h2>
	<div class="main-box rounded">
		<h3>Contractor Rules &amp; Regulations</h3>
		<p>Unit owners are responsible for any activity done by any outside contractors to their individual units. The following are a reminder of the rules for contractors and owners:</p>
		<ol>
			<li>The hours for renovation work on your unit is from 8 A.M. to 6 P.M Mondays through Saturday. No work is permitted Sundays or Holidays.</li>
			<li>Contractors must enter and exit through the loading doors and all material must come in and out through the loading doors.</li>
			<li>Owners must be available to allow entry into the building for the contractors when they arrive and depart.</li>
			<li>When a contractor has completed unloading or loading their vehicle it must be removed from the loading door and parked in visitors parking to allow other residents use of the loading doors. </li>
			<li>Under no circumstances is there permitted any work of any nature in front of the loading doors or any portion of the visitors parking area  (cutting, material storage etc.) all work must be done in the individual condominium.</li>
			<li>No material is allowed to be stored in any hallway within the building as it is against the Fire Code.</li>
			<li>Owners are responsible for cleaning any debris or mess created in any hallway or elevator as a result of work being done in their units.</li>
			<li>Owners are responsible for any damage caused to any portion of the building (hallway walls, elevators, etc.) as a result of the actions of the contractor working on your unit.</li>
			<li>Elevator service is available for owners but must be reserved in advance with the Superintendent.</li>
			<li>No material of any nature is permitted to be placed in the condominium garbage area for disposal as a result of any delivery or renovation. It must be removed by the contractor or unit owner.</li>
		</ol>
		<h3>Your co-operation is appreciated</h3>

	</div>

</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".contractor").addClass("active");
		$(".info").addClass("active");
	});
</script>

