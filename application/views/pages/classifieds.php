<div class="content content-full">
<h2 class="top">Classified Ads</h2>
<?php
	if($advertisements)
	{

	foreach($advertisements as $ads)
	{
?>
		<div class="announceDiv">
			<h4 class="top"><?=$ads['adType'];?></h4>
			<strong><?=$ads['adTitle'];?></strong>
			<p><?=$ads['adBody'];?></p>
		</div>
<?php
	}
} else {
	echo '<h3 class="top">There are no classified advertisements at the moment. Check back later!</h3>';
}
?>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".info").addClass("active");
		$(".classifieds").addClass("active");
	});

	$("#filter").on("change",function(){
		alert($(this).val());
	})
</script>
