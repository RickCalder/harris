<div class="content">
<h2 class="top">Welcome to Harristowers.com</h2>
<p>
	Hamilton Ontario is the location of these two stunning Condominium buildings known as Harris Towers located at 30 and 40 Harrisford Street near Greenhill Avenue.
</p>
<p>
	The nearby Red Hill Parkway provides fast access to Downtown as well as the major highways. There are numerous ammenities available such as a full size saltwater swimming pool, tennis courts, excersise room, community party room and a library.
</p>
<p>
	This web site serves the Condo Owners and the Property Management Company, providing information about the building, its Bylaws and house rules, newsletters and contact with the Board of Directors.
</p>
<div class="" id="home-slider-container">
	<div id="home-slider">
		<ul class="slides">
			<li>
				<img class="rounded" src="<?=base_url();?>assets/images/slides/pool_slide.jpg" alt="" width="707" height="300" />
			</li>
			<li>
				<img class="rounded" src="<?=base_url();?>assets/images/slides/gym_slide.jpg" alt="" width="707" height="300" />
			</li>
			<li>
				<img class="rounded" src="<?=base_url();?>assets/images/slides/landscape_slide.jpg" alt="" width="707" height="300" />
			</li>
			<li>
				<img class="rounded" src="<?=base_url();?>assets/images/slides/library_slide.jpg" alt="" width="707" height="300" />
			</li>
			<li>
				<img class="rounded" src="<?=base_url();?>assets/images/slides/underground_slide.jpg" alt="" width="707" height="300" />
			</li>
			<li>
				<img class="rounded" src="<?=base_url();?>assets/images/slides/workshop_slide.jpg" alt="" width="707" height="300" />
			</li>
		</ul>
	</div>                
</div>

</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".home").addClass("active");
	});
</script>