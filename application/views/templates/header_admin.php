<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE, NO-STORE, must-revalidate">
	<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
	<META HTTP-EQUIV="EXPIRES" CONTENT=-1>
	<!--[if lt IE 9]>
	  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/reset.css?time=<?php echo filemtime('./assets/css/reset.css');?>" />
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main.css?time=<?php echo filemtime('./assets/css/main.css');?>" />
	<script type="text/javascript" src="<?=base_url();?>assets/lib/jquery-1.8.3.min.js"></script>
    <link rel="SHORTCUT ICON" href="<?php echo base_url(); ?>assets/images/favicon.png" />

	<?=$additionalHeadInfo;?>
</head>
	<body>
	<div class="wrapper"><!-- needed to stick footer-->
	<div id="mainnav">
		<div class="container">
		<div id="logged" class="right">
			<p>Welcome <?=$this->session->userdata('firstName');?>&nbsp;|&nbsp;<a href="<?=base_url();?>logout">Log Out</a></p>
		</div>
			<ul>
				<!--<li class="first has-arrow"><a href="<?php echo base_url(); ?>index">Home <img src="<?php echo base_url(); ?>assets/images/mainnav-arrow.png" width="8" height="5" /></a></li>-->
				<li class="first home"><a href="<?php echo base_url(); ?>">Home</a></li>
				<li class="has-arrow info"><a href="">Info <img src="<?php echo base_url(); ?>assets/images/mainnav-arrow.png" width="8" height="5" /></a>
					<ul>
						<li class="contractor"><a href="<?php echo base_url(); ?>contractor" class="last">Contractor Rules</a></li>
					</ul>
				</li>
				<li class="contact"><a href="<?php echo base_url(); ?>contact">Contact</a></li>
				<?php
					if (in_array('basic_user',$this->session->userdata('roleNames'))) {
				?>

				<li class="has-arrow owners"><a href="/owners_home">Owners <img src="<?=base_url();?>assets/images/mainnav-arrow.png" width="8" height="5" /></a>
				<ul>
					<li class="newslettersmenu"><a href="#">Newsletters ></a>
						<ul>
							<li class="newsletters"><a href="<?php echo base_url(); ?>newsletters">Current Newsletter</a></li>
							<li class="newsletters_archive"><a href="<?php echo base_url(); ?>newsletters_archive" class="last">Newsletters Archive</a></li>
						</ul>
					</li>
					<li class="periodicsmenu"><a href="#">Periodic Information Certificates ></a>
						<ul>
							<li class="periodics"><a href="<?php echo base_url(); ?>periodics">Current PIC</a></li>
							<li class="periodics_archive"><a href="<?php echo base_url(); ?>periodics_archive" class="last">Pic Archive</a></li>
						</ul>
					</li>
					<li class="owners_gallery"><a href="<?php echo base_url(); ?>galleries">Photo Galleries</a></li>
					<li class="whats_happening"><a href="#">What's Happening ></a>
						<ul>
							<li class="owners_calendar"><a href="<?php echo base_url(); ?>events">Event Calendar</a></li>
							<li class="owners_announcements"><a href="<?php echo base_url(); ?>announce">Announcements</a></li>
							<li class="classifieds"><a href="<?php echo base_url(); ?>classifieds" class="last">Classified Ads</a></li>
						</ul>
					</li>
					<li><a href="#" title="All outside links open in a new tab/window">Outside Links</a>
						<ul>
							<li><a href="http://www.e-laws.gov.on.ca/html/statutes/english/elaws_statutes_98c19_e.htm" target ="_blank" class="last">Condominium Act 1998</a></li>
						</ul>
					</li>
					<li class="owners_contact"><a href="<?php echo base_url(); ?>owners_contact">Building Contacts</a></li>
					<li class="approved_contractors"><a href="<?php echo base_url(); ?>approved_contractors">Approved Contractors</a></li>
					<li><a href="<?php echo base_url(); ?>assets/pdfs/handbook.pdf" target="_blank">Resident Handbook</a></li>
					<li class="feedback"><a href="<?php echo base_url(); ?>feedback" class="last">Website Feedback</a></li>
				</ul>
				</li>
				<?php
					}
					if (in_array('basic_admin',$this->session->userdata('roleNames'))) {
				?>
				<li class="has-arrow owners"><a href="/owners_home">Owners <img src="<?=base_url();?>assets/images/mainnav-arrow.png" width="8" height="5" /></a>
				<ul>
					<li class="newslettersmenu"><a href="#">Newsletters ></a>
						<ul>
							<li class="newsletters"><a href="<?php echo base_url(); ?>newsletters">Current Newsletter</a></li>
							<li class="newsletters_archive"><a href="<?php echo base_url(); ?>newsletters_archive" class="last">Newsletters Archive</a></li>
						</ul>
					</li>
					<li class="periodicsmenu"><a href="#">PICs ></a>
						<ul>
							<li class="periodics"><a href="<?php echo base_url(); ?>periodics">Current PIC</a></li>
							<li class="periodics_archive"><a href="<?php echo base_url(); ?>periodics_archive" class="last">Pic Archive</a></li>
						</ul>
					</li>
					<li class="owners_gallery"><a href="<?php echo base_url(); ?>galleries">Photo Galleries</a></li>
					<li class="whats_happening"><a href="#">What's Happening ></a>
						<ul>
							<li class="owners_calendar"><a href="<?php echo base_url(); ?>events">Event Calendar</a></li>
							<li class="owners_announcements"><a href="<?php echo base_url(); ?>announce">Announcements</a></li>
							<li class="classifieds"><a href="<?php echo base_url(); ?>classifieds" class="last">Classified Ads</a></li>
						</ul>
					</li>
					<li><a href="#" title="All outside links open in a new tab/window">Outside Links</a>
						<ul>
							<li><a href="http://www.e-laws.gov.on.ca/html/statutes/english/elaws_statutes_98c19_e.htm" target ="_blank" class="last">Condominium Act 1998</a></li>
						</ul>
					</li>
					<li class="owners_contact"><a href="<?php echo base_url(); ?>owners_contact">Building Contacts</a></li>
					<li class="approved_contractors"><a href="<?php echo base_url(); ?>approved_contractors">Approved Contractors</a></li>
					<li><a href="<?php echo base_url(); ?>assets/pdfs/handbook.pdf" target="_blank">Resident Handbook</a></li>
					<li class="feedback"><a href="<?php echo base_url(); ?>feedback" class="last">Website Feedback</a></li>
				</ul>
				</li>
					<li class="has-arrow admin"><a href="<?=base_url();?>index">Admin <img src="<?=base_url();?>assets/images/mainnav-arrow.png" width="8" height="5" /></a>
					<ul>
						<li class="users"><a href="<?php echo base_url(); ?>users">Users ></a>
							<ul>
								<li class="list_users"><a href="<?php echo base_url(); ?>users">List Users</a></li>
								<li class="new_user"><a href="<?php echo base_url(); ?>new_user">Create User</a></li>
								<li class="board"><a href="<?php echo base_url(); ?>admin_board" class="last">List Board</a></li>
							</ul>
						</li>
						<li class="newslettersmenuadmin"><a href="#">Newsletters &gt;</a>
							<ul>
								<li class="newsletter_create"><a href="<?php echo base_url(); ?>newsletter_edit">Create Newsletters</a></li>
								<li class="newsletter_list"><a href="<?php echo base_url(); ?>newsletter_list" class="last">Manage Newsletters</a></li>
							</ul>
						</li>
						<li class="adminImageGallery"><a href="#">Image Galleries ></a>
							<ul>
								<li class="adminImageGallery"><a href="<?php echo base_url(); ?>admin_image_gallery" class="last">Add/Edit Galleries</a></li>
							</ul>
						</li>
						<li class="notifications"><a href="#" class="last">Site Maintenance &gt;</a>
							<ul>
								<li class="admin_events"><a href="<?php echo base_url(); ?>admin_events">Calendar Events</a></li>
								<li class="admin_advertisements"><a href="<?php echo base_url(); ?>admin_advertisements">Advertisements</a></li>
								<li class="admin_announcements"><a href="<?php echo base_url(); ?>announcements">Announcements</a></li>
								<li class="admin_contractors"><a href="<?php echo base_url(); ?>admin_contractors">Approved Contractors</a></li>
								<li class="add_contractor_type"><a href="<?php echo base_url(); ?>add_contractor_type" class="last">Contractor Types</a></li>
							</ul>
						</li>

					</ul>
					</li>
				<?php
					}
				?>
			</ul>
		</div>
	</div><!--end main nav-->
	<div class="container"><!--closes in footer-->
	<div class="header">

		<div class="logo"><a href="<?=base_url();?>">Harris Towers</a></div>
	</div>
		<div class="clear"></div>
	</div>
	<div class="container"><!--closes in footer-->
	  <?php

			$errors = $this->session->getErrors();

			if ($errors != null) {
				echo '<div class="sysError rounded">';
				echo implode("<br/>", $errors);
				echo '</div>';
			}

			$msgs = $this->session->getMessages();

			if ($msgs != null) {
				echo '<div class="sysMessage rounded">';
				echo implode("<br/>", $msgs);
				echo '</div>';
			}

		?>