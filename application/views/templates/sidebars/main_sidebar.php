<div class="sidebar">

<?php

	if(!$this->session->userdata('isLoggedIn'))

	{

?>

	<div class="side-box rounded center">

		<h3 class="top">Owner Login</h3>

		<?php

			echo form_open('login');

			echo '<label class="label-login">User Name</label>';

			$formdata = array(

				'name'=>'userName',

				'id'=>'userName',

			);

			echo form_input($formdata);



			echo '<label class="label-login">Password</label>';

			$formdata = array(

				'name'=>'password',

				'id'=>'password',

			);

			echo form_password($formdata);



			$formdata = array(

				'name'=>'submit',

				'id'=>'submit',

				'value'=>'Login',

				'class'=>'button'

			);

			echo form_submit($formdata);

		?>

	</div>


<?php

	} else {

?>


<?php

	if($this->session->userdata('isLoggedIn') && $this->announcements)

	{

?>

	<div class="side-box rounded">

		<?php

			foreach($this->announcements as $announce)

			{

		?>

			<p><strong><?=$announce['announcementTitle'];?></strong></p>

			<p><?=$announce['announcementBody'];?></p>

			<hr />

		<?php

			}

		?>

		<p class="center"><a href="<?=base_url();?>announce">See all announcements</a></p>

	</div>

<?php

	}

?>

<?php

	}

	if(isset($this->ads) && $this->session->userdata('isLoggedIn') && $this->ads)

	{

?>



	<div class="side-box rounded">

		<?php

			foreach($this->ads as $ad)

			{

				echo '<h4 class="top center">'.$ad['adType'].'</h4>';

				echo '<p><strong>'.$ad['adTitle'].'</strong></p>';

				echo '<p>'.$ad['adBody'].'</p>';

				echo '<hr />';

			}

		?>

		<p class="center"><a href="<?=base_url();?>classifieds">See all advertisements</a></p>

	</div>

	<?php

	}

	?>



<?php

	$public = 0;



	if(isset($this->ads) && !$this->session->userdata('isLoggedIn') && $this->ads)

	{

		foreach($this->ads as $ad)

		{

			if($ad['adPublic']==1)

			{

				$public ++;

			}

		}

	}

	if($public > 0)

	{

?>



	<div class="side-box rounded">

		<h3 class="top">Advertisements</h3>

		<?php

			foreach($this->ads as $ad)

			{

				if($ad['adPublic']==1)

				{

					echo '<h4 class="top center">'.$ad['adType'].'</h4>';

					echo '<p><strong>'.$ad['adTitle'].'</strong></p>';

					echo '<p>'.$ad['adBody'].'</p>';

					echo '<hr />';

				}

			}

		?>

		<p class="center"><a href="<?=base_url();?>public_classifieds">See all advertisements</a></p>

	</div>

	<?php

	}

	?>

</div>