	
	

	</div><!--closes container that opens in header-->	
	<div class="push"></div>
	</div><!--closes wrapper-->
		<div class="footer">
			<div class="container center">
				<p>&copy;<?php echo date('Y'); ?> Harris Towers. All rights reserved. Design and development by <a href="mailto:rick@calderonline.com">CalderOnline.com</a></p>
			</div>
		</div>
	</body>
</html>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-39707786-1', 'harristowers.com');
  ga('send', 'pageview');

</script>