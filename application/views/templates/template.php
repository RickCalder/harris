<?php 
	if($this->session->userdata('isLoggedIn'))
	{
		$this->load->view('templates/header_admin', $title);
	} else  {
		$this->load->view('templates/header', $title);
	}
	$this->load->view('templates/'.$sidebar);
	$this->load->view('pages/'.$mainContent);
	$this->load->view('templates/footer'); 