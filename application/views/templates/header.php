<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title ?></title>
	<meta charset="utf-8">
	<!--[if lt IE 9]>
	  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/reset.css?time=<?php echo filemtime('./assets/css/reset.css');?>" />
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main.css?time=<?php echo filemtime('./assets/css/main.css');?>" />
	<script type="text/javascript" src="<?=base_url();?>assets/lib/jquery-1.8.3.min.js"></script>
    <link rel="SHORTCUT ICON" href="<?php echo base_url(); ?>assets/images/favicon.png" />
	<?=$additionalHeadInfo;?>
</head>
	<body>
	<div class="wrapper"><!-- needed to stick footer-->
	<div id="mainnav">
		<div class="container">
			<ul>
				<!--<li class="first has-arrow"><a href="<?php echo base_url(); ?>index">Home <img src="<?php echo base_url(); ?>assets/images/mainnav-arrow.png" width="8" height="5" /></a></li>-->
				<li class="first home"><a href="<?php echo base_url(); ?>">Home</a></li>
				<li class="has-arrow info"><a href="">Info <img src="<?php echo base_url(); ?>assets/images/mainnav-arrow.png" width="8" height="5"/></a>
					<ul>
						<li class="contractor"><a href="<?php echo base_url(); ?>contractor">Contractor Rules</a></li>
						<li class="classifieds"><a href="<?php echo base_url(); ?>public_classifieds" class="last">Classified Ads</a></li>
					</ul>
				</li>
				<li class="contact"><a href="<?php echo base_url(); ?>contact">Contact</a></li>
			</ul>
		</div>
	</div>
	<div class="container"><!--closes in footer-->
	<div class="header">
		<div class="logo"><a href="<?=base_url();?>">Harris Towers</a></div>
	</div>
		<div class="clear"></div>
	</div>
	<div class="container"><!--closes in footer-->
	  <?php
			$errors = $this->session->getErrors();
			if ($errors != null) {
				echo '<div class="sysError rounded">';
				echo implode("<br/>", $errors);
				echo '</div>';
			}
			$msgs = $this->session->getMessages();
			if ($msgs != null) {
				echo '<div class="sysMessage rounded">';
				echo implode("<br/>", $msgs);
				echo '</div>';
			}
		?>