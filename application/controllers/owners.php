<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Owners extends User_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->model('newsletters');
    $this->load->model('advertisements');
    $this->load->model('announcements','announce_model');
	}

	public function approved_contractors()
	{
		$this->load->model('contractors');
		$this->load->model('contractor_types');
		$post = $this->input->postArray();
		if($post)
		{
			$data['contractors']=$this->contractors->getContractors(null,1,$post['contractorType']);
		} else {
			$data['contractors']=$this->contractors->getContractors(null,1,null);
		}
		$data['contractorTypes']=$this->contractor_types->contractorDropdown();
		$data['mainContent'] = 'owners/approved_contractors';
		$data['title']='Harris Towers - Approved Contractors';
		$data['additionalHeadInfo']='';
		$this->load->view('templates/template-full',$data);
	}

	public function newsletters()
	{
		$newsletterId=$this->uri->segment(2);
		if(!$newsletterId)
		{
			$newsletterId=0;
		}
		if (in_array('basic_admin',$this->session->userdata('roleNames')))
		{
			$active=0;
		} else {
			$active = 1;
		}
		$data['newsletters']=$this->newsletters->getNewsletters($newsletterId,$active);
		$data['newslettersDrop']=$this->newsletters->listNewslettersDropdown();
		$data['mainContent'] = 'owners/newsletters';
		$data['title']='Harris Towers - Newsletters';
		$data['additionalHeadInfo']='';
		$this->load->view('templates/template-full',$data);
	}

	public function periodics()
	{
		$newsletterId=$this->uri->segment(2);
		if(!$newsletterId)
		{
			$newsletterId=0;
		}
		if (in_array('basic_admin',$this->session->userdata('roleNames')))
		{
			$active=0;
		} else {
			$active = 1;
		}
		$data['newsletters']=$this->newsletters->getPeriodic($newsletterId,$active);
		$data['newslettersDrop']=$this->newsletters->listPeriodicDropdown();
		$data['mainContent'] = 'owners/periodic';
		$data['title']='Harris Towers - Periodic Information Certificates';
		$data['additionalHeadInfo']='';
		$this->load->view('templates/template-full',$data);
	}

  public function newsletters_archive()
  {
    $data['mainContent'] = 'owners/newsletters_list';
    $data['sidebar']='sidebars/main_sidebar';
    $data['newsletters']=$this->newsletters->getNewsletters();
    $data['title']='Harris Towers - Newsletters';
    $data['additionalHeadInfo']='';
    $this->load->view('templates/template',$data);
  }

  public function periodics_archive()
  {
    $data['mainContent'] = 'owners/periodic_list';
    $data['sidebar']='sidebars/main_sidebar';
    $data['newsletters']=$this->newsletters->getPeriodic();
    $data['title']='Harris Towers - Newsletters';
    $data['additionalHeadInfo']='';
    $this->load->view('templates/template',$data);
  }

  public function owners_landing()
  {
    $data['mainContent'] = 'owners/owners_landing';
    $data['sidebar']='sidebars/main_sidebar';
    $data['newsletters']=$this->newsletters->getNewsletters();
    $data['advertisements'] = $this->advertisements->getAdsForDisplay();
    $data['announcements'] = $this->announce_model->getAnnounceForMainPage();
    $data['title']='Harris Towers - Owners';
    $data['additionalHeadInfo']='';
    $this->load->view('templates/template-full',$data);
  }

	public function book_party()
	{
		$post = $this->input->postArray();
		if(isset($post) && $post)
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('eventTitle','trim|required');
			$this->form_validation->set_rules('eventData','trim|required');
			$this->form_validation->set_rules('requestEmail','trim|required');
			$this->form_validation->set_rules('eventDate','trim|required');
			$this->load->model('users');
			$data['user'] = $this->users->getUsers($this->session->userdata('userId'));
			if($this->form_validation->run()==false)
			{
				$this->load->model('users');
				$data['user'] = $this->users->getUsers($this->session->userdata('userId'));
				$data['mainContent'] = 'owners/book_party';
				$data['sidebar']='sidebars/main_sidebar';
				$data['newsletters']=$this->newsletters->getNewsletters();
				$data['title']='Harris Towers - Newsletters';
				$data['additionalHeadInfo']='
					<script type="text/javascript" src="'.base_url().'assets/lib/jquery-ui-1.8.22.custom.min.js"></script>
					<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
						';
				$this->load->view('templates/template-full',$data);
			} else {
				$this->load->model('events');
				$post['eventActive'] = 0;
				$post['eventPartyRoom'] = 1;
				$post['eventDisplay']=1;
				$post['eventTitle'] = 'Party Room - '.$post['eventTitle'];
				$this->events->loadFromArray($post);
				$this->events->save();
				$this->session->addMessage('Request sent, you will be contacted shortly!');
				$message = 'There has been a new party room event booked, please administer it!';
				$subject = 'New party room request';
				$this->sendEmail($message,$subject);
				$userMessage = 'Thank you for your request to book the party room. Please contact the Super to finalize the booking';
				$userSubject = 'Harris Towers party room booking';
				$this->sendUserEmail($userMessage,$userSubject,$data['user'][0]['emailAddress']);
				redirect(base_url().'events');
			}
		}
		$this->load->model('users');
		$data['user'] = $this->users->getUsers($this->session->userdata('userId'));
		$data['mainContent'] = 'owners/book_party';
		$data['sidebar']='sidebars/main_sidebar';
		$data['newsletters']=$this->newsletters->getNewsletters();
		$data['title']='Harris Towers - Newsletters';
		$data['additionalHeadInfo']='
			<script type="text/javascript" src="'.base_url().'assets/lib/jquery-ui-1.8.22.custom.min.js"></script>
			<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
				';
		$this->load->view('templates/template-full',$data);
	}

	public function aj_check_date()
	{
		$data = $this->input->postArray();
		$this->load->model('events');
		if($this->events->checkDate($data['eventDate'])===1)
		{
			print json_encode(array('status'=>'success','message'=>'Sorry this date is booked already.','cssClass'=>'red'));
		} else {
			print json_encode(array('status'=>'success','message'=>'This date is available','cssClass'=>'green'));
		}
	}
	public function announce()
	{
		$this->load->model('announcements','announce_model');
		$data['announcements'] = $this->announce_model->getAnnounceForMainPage();
		$data['mainContent'] = 'owners/announcements';
		$data['title']='Harris Towers - Announcements';
		$data['additionalHeadInfo']='';
		$this->load->view('templates/template-full',$data);
	}

	public function classifieds()
	{
		$this->load->model('advertisements');
		$data['advertisements'] = $this->advertisements->getAdsForDisplay();
		$data['mainContent'] = 'owners/classifieds';
		$data['title']='Harris Towers - Classified Ads';
		$data['additionalHeadInfo']='';
		$this->load->view('templates/template-full',$data);
	}

	public function owners_contact()
	{
		$this->load->model('board');
		$data['board'] = $this->board->getMembers();
		$data['mainContent'] = 'owners/owners_contact';
		$data['title']='Harris Towers - Building Contacts';
		$data['additionalHeadInfo']='';
		$this->load->view('templates/template-full',$data);
	}

	public function feedback()
	{
		$data['mainContent'] = 'owners/feedback';
		$data['title']='Harris Towers - Website Feedback';
		$data['additionalHeadInfo']='';
		$this->load->view('templates/template-full',$data);
	}

	public function feedback_form()
	{
		$data=$this->input->postArray();
		$message =
		'From: '.$data['name'].'<br />'.
		'Email: '.$data['email'].'<br />'.
		'Subject: '.$data['subject'].'<br />'.
		'Comments: '.$data['comments'].'<br />';
		$subject="New Harris Towers website feedback from ".$data['name'];
		$this->sendEmail($message, $subject);
		$this->feedback();
	}

	function galleries()
	{
		$this->load->model('photo_galleries');
		$data['galleries']=$this->photo_galleries->getAllGalleries(0);
		$data['mainContent'] = 'owners/galleries';
		$data['sidebar']='sidebars/main_sidebar';
		$data['title']='Harris Towers - Photo Galleries';
		$data['additionalHeadInfo'] = '
			 <link rel="stylesheet" href="'.base_url().'assets/css/prettyPhoto.css" type="text/css" media="screen" />
			<script type="text/javascript" charset="utf-8" src="'.base_url().'assets/lib/jquery.prettyPhoto.js"></script>
			<script type="text/javascript">
				$(document).ready(function () {
					$("a[rel^=\'prettyPhoto\']").prettyPhoto({social_tools:false, deeplinking:false});
				});
			</script>
		';
		$this->load->view('templates/template',$data);

	}

	function view_gallery()
	{
		$galleryId = $this->uri->segment(2);
		$this->load->model('photo_galleries');
		$data['gallery'] = $this->photo_galleries->getGallery($galleryId,0);
		$data['mainContent'] = 'owners/view_gallery';
		$data['sidebar']='sidebars/main_sidebar';
		$data['title']='Harris Towers - Image Gallery';
		$data['additionalHeadInfo'] = '
			 <link rel="stylesheet" href="'.base_url().'assets/css/prettyPhoto.css" type="text/css" media="screen" />
			<script type="text/javascript" charset="utf-8" src="'.base_url().'assets/lib/jquery.prettyPhoto.js"></script>
			<script type="text/javascript">
				$(document).ready(function () {
					$("a[rel^=\'prettyPhoto\']").prettyPhoto({social_tools:false, deeplinking:false});
				});
			</script>
		';
		$this->load->view('templates/template-full',$data);
	}

	function events()
	{
		if($this->uri->segment(2))
		{
			$year = $this->uri->segment(2);
			$month = $this->uri->segment(3);
		} else {
			$year = null;
			$month = null;
		}
		$config = array(
			'show_next_prev'=>true,
			'next_prev_url'=>base_url().'events',
			'day_type'=>'long'
		);
		$this->load->model('events');
		$cal_data=$this->events->getCalendarData($year, $month);
		$config['template'] = '

		   {table_open}<table border="0" cellpadding="0" cellspacing="0" class="calendar">{/table_open}

		   {heading_row_start}<tr>{/heading_row_start}

		   {heading_previous_cell}<th><a href="{previous_url}" class="left_arrow"></a></th>{/heading_previous_cell}
		   {heading_title_cell}<th colspan="{colspan}" class="month">{heading}</th>{/heading_title_cell}
		   {heading_next_cell}<th><a href="{next_url}" class="right_arrow"></a></th>{/heading_next_cell}

		   {heading_row_end}</tr>{/heading_row_end}

		   {week_row_start}<tr class="week">{/week_row_start}
		   {week_day_cell}<td>{week_day}</td>{/week_day_cell}
		   {week_row_end}</tr>{/week_row_end}

		   {cal_row_start}<tr class="days">{/cal_row_start}
		   {cal_cell_start}<td class="thisDay">{/cal_cell_start}

		   {cal_cell_content}
		   	<div class="day_num">{day}</div>
		   	<div class="cal_content">{content}</div>
		   {/cal_cell_content}

		   {cal_cell_content_today}
		   	<div class="day_num highlight">{day}</div>
		   	<div class="cal_content">{content}</div>
		   	<div class="cal_data"></div>
		   {/cal_cell_content_today}

		   {cal_cell_no_content}
		   	<div class="day_num">{day}</div>
		   {/cal_cell_no_content}

		   {cal_cell_no_content_today}
		   	<div class="day_num highlight">{day}</div>
		   {/cal_cell_no_content_today}

		   {cal_cell_blank}<div class="notDay">&nbsp;</div>{/cal_cell_blank}

		   {cal_cell_end}</td>{/cal_cell_end}
		   {cal_row_end}</tr>{/cal_row_end}

		   {table_close}</table>{/table_close}
		';
		$this->load->library('calendar',$config);
		$data['calendar']=$this->calendar->generate($year,$month,$cal_data);
		$data['mainContent'] = 'owners/events';
		$data['sidebar']='';
		$data['title']='Harris Towers - Calendar of events';
		$data['additionalHeadInfo'] = '
			 <link rel="stylesheet" href="'.base_url().'assets/css/prettyPhoto.css" type="text/css" media="screen" />
			<script type="text/javascript" charset="utf-8" src="'.base_url().'assets/lib/jquery.prettyPhoto.js"></script>
			<script type="text/javascript">
				$(document).ready(function () {
					$("a[rel^=\'prettyPhoto\']").prettyPhoto({social_tools:false, deeplinking:false});
				});
			</script>
		';
		$this->load->view('templates/template-full',$data);

	}

	function ajGetEvent()
	{

		$data=$this->input->getArray();
		$this->db->where('eventId',$data['eventId']);
		$this->db->select('eventData');
		$data=$this->db->get('events');
		$result = $data->result_array();
		print json_encode(array('status'=>'success','message'=>$result[0]['eventData']));
	}

	public function owner_advertisements()
	{
		$this->load->model('advertisements');
		$data['advertisements']=$this->advertisements->getAdvertisements(0,$this->session->userdata('userId'));
		$this->load->model('ad_types');
		$prefixSelect = array('0'=>'--Select Type of Ad');
		$data['adTypes']=$this->ad_types->getAdTypes();
		$data['adTypes']=$prefixSelect + $data['adTypes'];
		$post = $this->input->postArray();
		if($post)
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('adTitle','Ad Title','trim|required');
			$this->form_validation->set_rules('adBody','Ad Body','trim|required');
			$this->form_validation->set_rules('adTypeId','Ad Type','trim|required');
			if($this->form_validation->run()==FALSE)
			{
				$data['mainContent'] = 'owners/owner_advertisements';
				$data['title']='Harris Towers - Advertisements';
				$data['additionalHeadInfo']='
					<script type="text/javascript" src="'.base_url().'assets/lib/jquery-ui-1.8.22.custom.min.js"></script>
					<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
			 <script type="text/javascript" src="'.base_url().'assets/lib/jHtmlArea-0.7.5.min.js"></script>
				';
				$this->load->view('templates/template-full',$data);
			} else {
				$post['display'] = (in_array('basic_admin',$this->session->userdata('roleNames'))? 1 : 0);
				$post['adActive']=1;
				$post['adExpires'] = date('Y-m-d',strtotime('+2 weeks'));
				$post['createdDateTime']=date('Y-m-d',time());

				//$this->printCheck($post);
				$this->advertisements->loadFromArray($post);
				$this->advertisements->save();
			$message = 'There has been a new advertisement placed, please administer it!';
			$subject = 'New advertisement';
			$this->sendEmail($message,$subject);
				$this->session->addMessage('Advertisement saved!');
				redirect('post_ad');
		}
			}
		$data['mainContent'] = 'owners/owner_advertisements';
		$data['title']='Harris Towers - Advertisements';
		$data['additionalHeadInfo']='
			<script type="text/javascript" src="'.base_url().'assets/lib/jquery-ui-1.8.22.custom.min.js"></script>
			<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
			 <script type="text/javascript" src="'.base_url().'assets/lib/jHtmlArea-0.7.5.min.js"></script>
		';
		$this->load->view('templates/template-full',$data);
	}
	public function edit_ad()
	{
		if($this->uri->segment(2))
		{
			$adId = $this->uri->segment(2);
		} else {
			$adId = 0;
		}
		$this->load->model('advertisements');
		$data['advertisement']=$this->advertisements->getAdvertisements($adId,$this->session->userdata('userId'));
		$this->load->model('ad_types');
		$prefixSelect = array('0'=>'--Select Type of Ad');
		$data['adTypes']=$this->ad_types->getAdTypes();
		$data['adTypes']=$prefixSelect + $data['adTypes'];
		$post = $this->input->postArray();
		if($post)
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('adTitle','Ad Title','trim|required');
			$this->form_validation->set_rules('adBody','Ad Body','trim|required');
			$this->form_validation->set_rules('adTypeId','Ad Type','trim|required');
			if($this->form_validation->run()==FALSE)
			{
				$data['mainContent'] = 'admin/edit_advertisement/'.$post['adId'];
				$data['title']='Harris Towers - Advertisements';
				$data['additionalHeadInfo']='
					<script type="text/javascript" src="'.base_url().'assets/lib/jquery-ui-1.8.22.custom.min.js"></script>
					<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
				';
				$this->load->view('templates/template-full',$data);
			} else {
				$post['display'] = (in_array('basic_admin',$this->session->userdata('roleNames'))? 1 : 0);
				$post['adActive']=1;
				$post['adExpires'] = date('Y-m-d',strtotime('+2 weeks'));
				$post['createdDateTime']=date('Y-m-d',time());

				//$this->printCheck($post);
				$this->advertisements->loadFromArray($post);
		//$this->printCheck($post);
				$this->advertisements->save();

				$this->session->addMessage('Advertisement saved!');
				redirect('post_ad');
		}
			}
		$data['mainContent'] = 'owners/edit_my_ad';
		$data['title']='Harris Towers - Advertisements';
		$data['additionalHeadInfo']='
			<script type="text/javascript" src="'.base_url().'assets/lib/jquery-ui-1.8.22.custom.min.js"></script>
			<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
			 <script type="text/javascript" src="'.base_url().'assets/lib/jHtmlArea-0.7.5.min.js"></script>

		';
		$this->load->view('templates/template-full',$data);
	}
}
