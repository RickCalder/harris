<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->model('users');
		$this->load->model('newsletters');
		$this->load->model('announcements','announceModel');
		$this->load->model('events');
	}

	public function add_contractor_type(){
		$post = $this->input->postArray();
		$this->load->model('contractor_types');
		if($post)
		{
			//$this->printCheck($post);
			$this->load->library('form_validation');
			$this->form_validation->set_rules('contractorTypeName','Type','trim|required|is_unique[contractor_types.contractorTypeName');
			if($this->form_validation->run()==FALSE)
			{
			} else {
				$post['contractorTypeActive'] = 1;
				$this->contractor_types->loadFromArray($post);
				$this->contractor_types->save();
			}

		}
		$data['contractorTypes']=$this->contractor_types->getContractorTypes();
		$data['mainContent'] = 'admin/admin_contractor_types';
		$data['sidebar']='sidebars/main_sidebar';
		$data['title']='Harris Towers - Contractor Types';
		$data['additionalHeadInfo'] = '';
		$this->load->view('templates/template-full',$data);
	}

	function aj_delete_contractor_type()
	{
		$post = $this->input->post();
		$this->db->where('contractorTypeId',$post['contractorTypeId']);
		$array = array('contractorTypeActive'=>999);
		$this->db->update('contractor_types',$array);
		if($this->db->affected_rows()==1)
		{
			print json_encode(array("status"=>"success", "message"=>''));
		} else {
			print json_encode(array("status"=>"fucker", "message"=>''));
		}
	}

	public function admin_contractors()
	{
		$post = $this->input->postArray();
		$this->load->model('contractors');
		$this->load->model('contractor_types');
		if($post)
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('contractorName','Name','trim|required');
			$this->form_validation->set_rules('contractorDescription','Description','trim|required');
			$this->form_validation->set_rules('contractorPhone','Phone','trim|required');
			$this->form_validation->set_rules('contractorType','Type','trim|required');
			if($this->form_validation->run()==FALSE)
			{
			} else {
				$this->contractors->loadFromArray($post);
				$this->contractors->save();
			}

		}
		$data['contractors']=$this->contractors->getContractors(null,null,null);
		$data['contractorTypes']=$this->contractor_types->contractorDropdown();
		$data['mainContent'] = 'admin/admin_contractors';
		$data['sidebar']='sidebars/main_sidebar';
		$data['title']='Harris Towers - Contractors';
		$data['additionalHeadInfo'] = '';
		$this->load->view('templates/template-full',$data);

	}
	public function edit_contractor()
	{
		$post = $this->input->postArray();
		$this->load->model('contractors');
		$this->load->model('contractor_types');
		if($post)
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('contractorName','Name','trim|required');
			$this->form_validation->set_rules('contractorDescription','Description','trim|required');
			$this->form_validation->set_rules('contractorPhone','Phone','trim|required');
			$this->form_validation->set_rules('contractorType','Type','trim|required');
			if($this->form_validation->run()==FALSE)
			{
			} else {
				$this->contractors->loadFromArray($post);
				$this->contractors->save();
				$this->session->addMessage('Contractor Updated.');
				redirect(base_url().'edit_contractor/'.$post['contractorId']);
			}

		}
		$data['contractor']=$this->contractors->getContractors($this->uri->segment(2),null,null);
		$data['contractorTypes']=$this->contractor_types->contractorDropdown();
		$data['mainContent'] = 'admin/edit_contractor';
		$data['sidebar']='sidebars/main_sidebar';
		$data['title']='Harris Towers - Contractors';
		$data['additionalHeadInfo']='';
		$this->load->view('templates/template-full',$data);

	}

	function aj_contractor_display()
	{
		$post=$this->input->postArray();
		if($post['contractorActive']==1)
		{
			$post['contractorActive']=0;
		} else {
			$post['contractorActive']=1;
		}
		$array = array('contractorActive'=>$post['contractorActive']);
		$this->db->where('contractorId',$post['contractorId']);
		$this->db->update('contractors',$array);
		//$this->printCheck($this->db->last_query());
		if($this->db->affected_rows()==1)
		{
			print json_encode(array("status"=>"success", "message"=>$post['contractorActive']));
		}
	}

	function aj_delete_contractor()
	{
		$post = $this->input->post();
		$this->db->where('contractorId',$post['contractorId']);
		$array = array('contractorActive'=>999);
		$this->db->update('contractors',$array);
		if($this->db->affected_rows()==1)
		{
			print json_encode(array("status"=>"success", "message"=>''));
		}
	}

	public function users()
	{
		$post = $this->input->post();
		if($post)
		{
			$this->session->set_userdata('building',$post['building']);
			$this->session->set_userdata('keyword',$post['keyword']);

		}
		if(! is_null($this->session->userdata('building')))
		{
			$building = $this->session->userdata('building');
			$keyword = $this->session->userdata('keyword');
		} else {
			$building = 0;
			$keyword=null;
		}
		if(isset($post['clear']))
		{
			$array=array('building'=>'','keyword'=>'');
			$this->session->unset_userdata($array);
			$building = 0;
			$keyword=null;
		}
		if($this->uri->segment(2))
		{
			$offset = $this->uri->segment(2);
		} else {
			$offset = 0;
		}
		$this->load->library('pagination');
		$config['base_url'] = base_url().'users';
		$config['total_rows'] =count($this->users->getUsers(0,$building,$offset,0,$keyword));
		$data['count'] = $config['total_rows'];
		$config['per_page'] = 25;
		$config['num_links'] = 3;
		$config['uri_segment'] = 2;
		$this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();

		$list = $this->users->getUsers(0,$building,$offset,$config['per_page'],$keyword);
		$users = array();
		foreach ($list as $line)
		{
			$user = clone($this->users);
			$user->loadFromArray($line);
			$users[]=$user;
		}
		$data['users'] = $users;
		$data['mainContent'] = 'admin/users';
		$data['sidebar']='sidebars/main_sidebar';
		$data['title']='Harris Towers - Users';
		$data['additionalHeadInfo']='';
		$this->load->view('templates/template-full',$data);
	}

	public function edit_user()
	{
		$userId = $this->uri->segment(3);
		$data['user'] = $this->users->getUser($userId);
		$this->printCheck($data);
	}

	public function new_user()
	{
		$post = $this->input->postArray();
		if(isset($post) && $post)
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('unitNumber', 'Unit Number','trim|required|numeric');
			$this->form_validation->set_rules('userName', 'User Name','trim|required|is_unique[users.userName]');
			$this->form_validation->set_rules('phone', 'Phone Number','trim|required');
			$this->form_validation->set_rules('emailAddress', 'Email','trim');
			$this->form_validation->set_rules('mobilePhone', 'Cell','trim');
			$this->form_validation->set_rules('alternatePhone', 'Alt','trim');
			$this->form_validation->set_rules('password', 'Password','trim|required|matches[verifyPassword]');
			$this->form_validation->set_rules('verifyPassword', 'Verify Password','trim|required');
			if($this->form_validation->run()==FALSE)
			{
				$data['mainContent'] = 'admin/new_user';
				$data['sidebar']='sidebars/main_sidebar';
				$data['title']='Harris Towers - Create User';
				$data['additionalHeadInfo']='';
				$this->load->view('templates/template-full',$data);
			} else {
				$post['creationDateTime']= date('Y-m-d h:i:s',time());
				$post['active']=1;
				$post['passHash']=$post['password'];
				$post['password']=SHA1($post['password']);
				$this->load->model('users');
				$this->users->loadFromArray($post);
				$this->users->save();
				$post['userId'] = $this->users->get('userId');
				$this->load->model('people');
				$this->people->loadFromArray($post);
				$this->people->save();
				$data = array(
					'userId'=>$post['userId'],
					'roleId'=>2
				);
				$this->db->insert('user_roles',$data);
				$this->session->addMessage('User added successfully!');
				redirect('users');
			}
		} else {
			$data['mainContent'] = 'admin/new_user';
			$data['sidebar']='sidebars/main_sidebar';
			$data['title']='Harris Towers - Create User';
			$data['additionalHeadInfo']='';
			$this->load->view('templates/template-full',$data);
		}
	}

	public function admin_board()
	{
		$this->load->model('board');
		$data['board'] = $this->board->getMembers();
		$data['mainContent'] = 'admin/admin_board';
		$data['sidebar']='sidebars/main_sidebar';
		$data['title']='Harris Towers - Board Members';
		$data['additionalHeadInfo']='';
		$this->load->view('templates/template-full',$data);
	}

	public function image_gallery()
	{
		$this->load->model('photo_galleries');
		$data['galleries']=$this->photo_galleries->getAllGalleries();
		$post = $this->input->postArray();
		if(isset($post) && $post)
		{
			$post['galleryFolderName'] = strtolower(str_replace(' ','_',$post['galleryName']));
			$post['galleryActive']=1;
			$this->load->library('form_validation');
			if($post['galleryId']==0)
			{
				$this->form_validation->set_rules('galleryName','Gallery Name','trim|required|is_unique[photo_galleries.galleryName]');

				if($this->form_validation->run()==FALSE)
				{
					$data['mainContent'] = 'admin/image_gallery';
					$data['sidebar']='sidebars/main_sidebar';
					$data['title']='Harris Towers - Image Gallery';
					$data['additionalHeadInfo']='';
					$this->load->view('templates/template-full',$data);
				}
			}

			if($post['galleryId']==0)
			{
				mkdir('assets/images/galleries/'.$post['galleryFolderName'],0755,true);
			}
			$config = array(
				'upload_path'=> 'assets/images/galleries/'.$post['galleryFolderName'].'/',
				'allowed_types'=>'gif|jpg|png',
				'max_size'=>'4096',
				'overwrite'=>TRUE
			);
			$this->load->library('upload');
			$this->load->model('photo_galleries');
			$post['creationDateTime'] = date('Y-m-d h:i:s',time());
			if($post['galleryId']==0)
			{
				$post['galleryId']='';
			}
			$this->photo_galleries->loadFromArray($post);
			$this->photo_galleries->save();
			$galleryId = $this->photo_galleries->get('galleryId');
			$images = array();
			foreach($_FILES['image'] as $key => $value)
			{
				foreach($value as $key2 => $new)
				{
					$images[$key2][$key] = $new;
				}
			}
			foreach ($images as $image)
			{
				$data['image']=$image['name'];
				$data['galleryId'] = $galleryId;
				$data['imageActive']=1;
				$this->load->model('gallery_images');
				if($data['image'] !='')
				{
					$this->new_image = clone($this->gallery_images);
					$this->new_image->loadFromArray($data);
					$this->new_image->save();
				}
				if (!empty($image['name']) && $image['name'] !='')
				{
						 $this->imageUpload($image,$config['upload_path'],$image['name'],'all');
				}
				$this->session->addMessage('Images uploaded successfully!');
				redirect('admin_image_gallery');
			}
		} else {
			$data['mainContent'] = 'admin/image_gallery';
			$data['sidebar']='sidebars/main_sidebar';
			$data['title']='Harris Towers - Image Gallery';
			$data['additionalHeadInfo']='';
			$this->load->view('templates/template-full',$data);
		}
	}

	public function checkGalleryName()
	{
		$data = $this->input->getArray();
		if($data['galleryId'] == 0)
		{
			$this->db->where('galleryName',$data['galleryName']);
			$test = $this->db->get('photo_galleries');
			if($test->num_rows() > 0)
			{
				print json_encode(array('status'=>'error'));
			} else {
				print json_encode(array('status'=>'success'));
			}
		} else {
			print json_encode(array('status'=>'success'));
		}
		return false;
	}

	public function galleryActive()
	{
		$data = $this->input->getArray();
		$this->db->where('galleryId',$data['galleryId']);
		if($data['galleryActive']==1)
		{
			$data['galleryActive']=0;
		} else {
			$data['galleryActive']=1;
		}

		if($this->db->update('photo_galleries',$data))
		{
			print json_encode(array('status'=>'success','message'=>$data['galleryActive']));
		} else {
			print json_encode(array('status'=>'error'));
		}
	}

	function admin_view_gallery()
	{
		$galleryId = $this->uri->segment(2);
		$this->load->model('photo_galleries');
		$data['gallery'] = $this->photo_galleries->getGallery($galleryId,1);
		$data['mainContent'] = 'admin/admin_view_gallery';
		$data['sidebar']='sidebars/main_sidebar';
		$data['title']='Harris Towers - Image Gallery';
		$data['additionalHeadInfo'] = '
			 <link rel="stylesheet" href="'.base_url().'assets/css/prettyPhoto.css" type="text/css" media="screen" />
			<script type="text/javascript" charset="utf-8" src="'.base_url().'assets/lib/jquery.prettyPhoto.js"></script>
			<script type="text/javascript">
				$(document).ready(function () {
					$("a[rel^=\'prettyPhoto\']").prettyPhoto({social_tools:false, deeplinking:false});
				});
			</script>
		';
		$this->load->view('templates/template-full',$data);
	}

	function ajUpdateGallery()
	{
		$data = $this->input->getArray();
		if(isset($data['imageActive']) && $data['imageActive']=='on')
		{
			$data['imageActive']=1;
		} else {
			$data['imageActive']=0;
		}
		$this->db->where('galleryImageId',$data['galleryImageId']);
		$this->db->update('gallery_images',$data);
		if($this->db->affected_rows()>0)
		{
			print json_encode(array('status'=>'success'));
		}
	}

	function aj_update_board()
	{
		$post = $this->input->postArray();
		$post['boardActive']=1;
		switch ($post['boardTitle']) {
			case 'President':
				$post['priority']=1;
				break;
			case 'Vice President':
				$post['priority']=2;
				break;
			case 'Treasurer':
				$post['priority']=3;
				break;
			case 'Secretary':
				$post['priority']=4;
				break;
			case 'Director':
				$post['priority']=5;
				break;
			case 'Property Manager':
				$post['priority']=6;
				break;
			default:
				$post['priority']=6;
				break;
		}

		$this->load->model('board');
		$this->board->loadFromArray($post);
		if($this->board->save())
		{
			print json_encode(array("status"=>"success"));
		}
	}

	function aj_delete_board()
	{
		$post = $this->input->postArray();
		$this->load->model('board');
		$this->db->where('boardId',$post['boardId']);
		$array=array('boardActive'=>0);
		$this->db->update('board',$array);
		if($this->db->affected_rows()==1)
		{
			print json_encode(array("status"=>"success"));
		} else {
			print json_encode(array("status"=>"error","message"=>"Could not delete at this time"));
		}
	}

	public function announcements()
	{
		$data['announcements']=$this->announceModel->getAnnouncements();
		$post = $this->input->postArray();
		if($post)
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('announcementTitle','Announcement Title','trim|required');
			$this->form_validation->set_rules('announcementBody','Announcement Body','trim|required');
			$this->form_validation->set_rules('displayStart','Start Date','trim|required');
			$this->form_validation->set_rules('displayUntil','End Date','trim|required');
			if($this->form_validation->run()==FALSE)
			{
				$data['mainContent'] = 'admin/announcements';
				$data['title']='Harris Towers - Announcements';
				$data['additionalHeadInfo']='
					<script type="text/javascript" src="'.base_url().'assets/lib/jquery-ui-1.8.22.custom.min.js"></script>
					<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
				';
				$this->load->view('templates/template-full',$data);
			} else {
				$this->announceModel->loadFromArray($post);
				$this->announceModel->save();
				$this->session->addMessage('Announcement saved!');
				redirect('announcements');
			}
		}
		$data['mainContent'] = 'admin/announcements';
		$data['title']='Harris Towers - Announcements';
		$data['additionalHeadInfo']='
			<script type="text/javascript" src="'.base_url().'assets/lib/jquery-ui-1.8.22.custom.min.js"></script>
			<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
		';
		$this->load->view('templates/template-full',$data);
	}

	public function admin_advertisements()
	{
		$this->load->model('advertisements');
		$data['advertisements']=$this->advertisements->getAdvertisements();
		$this->load->model('ad_types');
		$prefixSelect = array('0'=>'--Select Type of Ad');
		$data['adTypes']=$this->ad_types->getAdTypes();
		$data['adTypes']=$prefixSelect + $data['adTypes'];
		$post = $this->input->postArray();
		if($post)
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('adTitle','Ad Title','trim|required');
			$this->form_validation->set_rules('adBody','Ad Body','trim|required');
			$this->form_validation->set_rules('adTypeId','Ad Type','trim|required');			if($this->form_validation->run()==FALSE)
			{
				$data['mainContent'] = 'admin/admin_advertisements';
				$data['title']='Harris Towers - Advertisements';
				$data['additionalHeadInfo']='
					<script type="text/javascript" src="'.base_url().'assets/lib/jquery-ui-1.8.22.custom.min.js"></script>
					<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
				';
				$this->load->view('templates/template-full',$data);
			} else {
				$post['display'] = (in_array('basic_admin',$this->session->userdata('roleNames'))? 1 : 0);
				$post['adActive']=1;
				$post['adExpires'] = date('Y-m-d',strtotime('+2 weeks'));
				$post['createdDateTime']=date('Y-m-d',time());

				//$this->printCheck($post);
				$this->advertisements->loadFromArray($post);
				$this->advertisements->save();
				$this->session->addMessage('Advertisement saved!');
				redirect('admin_advertisements');
		}
			}
		$data['mainContent'] = 'admin/admin_advertisements';
		$data['title']='Harris Towers - Advertisements';
		$data['additionalHeadInfo']='
			<script type="text/javascript" src="'.base_url().'assets/lib/jquery-ui-1.8.22.custom.min.js"></script>
			<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
			 <script type="text/javascript" src="'.base_url().'assets/lib/jHtmlArea-0.7.5.min.js"></script>

		';
		$this->load->view('templates/template-full',$data);
	}

	public function edit_advertisement()
	{
		if($this->uri->segment(2))
		{
			$adId = $this->uri->segment(2);
		} else {
			$adId = 0;
		}
		$this->load->model('advertisements');
		$data['advertisement']=$this->advertisements->getAdvertisements($adId);
		$this->load->model('ad_types');
		$prefixSelect = array('0'=>'--Select Type of Ad');
		$data['adTypes']=$this->ad_types->getAdTypes();
		$data['adTypes']=$prefixSelect + $data['adTypes'];
		$post = $this->input->postArray();
		if($post)
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('adTitle','Ad Title','trim|required');
			$this->form_validation->set_rules('adBody','Ad Body','trim|required');
			$this->form_validation->set_rules('adTypeId','Ad Type','trim|required');
			if($this->form_validation->run()==FALSE)
			{
				$data['mainContent'] = 'admin/edit_advertisement/'.$post['adId'];
				$data['title']='Harris Towers - Advertisements';
				$data['additionalHeadInfo']='
					<script type="text/javascript" src="'.base_url().'assets/lib/jquery-ui-1.8.22.custom.min.js"></script>
					<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
				';
				$this->load->view('templates/template-full',$data);
			} else {
				$post['display'] = (in_array('basic_admin',$this->session->userdata('roleNames'))? 1 : 0);
				$post['adActive']=1;
				$post['adExpires'] = date('Y-m-d',strtotime('+2 weeks'));
				$post['createdDateTime']=date('Y-m-d',time());

				//$this->printCheck($post);
				$this->advertisements->loadFromArray($post);
		//$this->printCheck($post);
				$this->advertisements->save();

				$this->session->addMessage('Advertisement saved!');
				redirect('admin_advertisements');
		}
			}
		$data['mainContent'] = 'admin/edit_advertisement';
		$data['title']='Harris Towers - Advertisements';
		$data['additionalHeadInfo']='
			<script type="text/javascript" src="'.base_url().'assets/lib/jquery-ui-1.8.22.custom.min.js"></script>
			<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
			 <script type="text/javascript" src="'.base_url().'assets/lib/jHtmlArea-0.7.5.min.js"></script>

		';
		$this->load->view('templates/template-full',$data);
	}

	public function edit_announcement()
	{
		$post = $this->input->postArray();
		if($post)
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('announcementTitle','Announcement Title','trim|required');
			$this->form_validation->set_rules('announcementBody','Announcement Body','trim|required');
			$this->form_validation->set_rules('displayStart','Start Date','trim|required');
			$this->form_validation->set_rules('displayUntil','End Date','trim|required');
			if($this->form_validation->run()==FALSE)
			{
				$data['mainContent'] = 'admin/announcements';
				$data['title']='Harris Towers - Announcements';
				$data['additionalHeadInfo']='
					<script type="text/javascript" src="'.base_url().'assets/lib/jquery-ui-1.8.22.custom.min.js"></script>
					<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
				';
				$this->load->view('templates/template-full',$data);
			} else {
				$this->announceModel->loadFromArray($post);
				$this->announceModel->save();
				$this->session->addMessage('Announcement saved!');
				redirect('announcements');
			}
		}
		$announcementId = $this->uri->segment(2);
		$data['announcement'] = $this->announceModel->getAnnouncements($announcementId);
		$data['mainContent'] = 'admin/edit_announcement';
		$data['title']='Harris Towers - Announcements';
		$data['additionalHeadInfo']='
			<script type="text/javascript" src="'.base_url().'assets/lib/jquery-ui-1.8.22.custom.min.js"></script>
			<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
		';
		$this->load->view('templates/template-full',$data);
	}

	function aj_delete_announce()
	{
		$post = $this->input->post();
		$this->db->where('announcementId',$post['announcementId']);
		$array = array('active'=>0);
		$this->db->update('announcements',$array);
		if($this->db->affected_rows()==1)
		{
			print json_encode(array("status"=>"success", "message"=>''));
		}
	}

	function aj_delete_ad()
	{
		$post = $this->input->post();
		$this->db->where('adId',$post['adId']);
		$array = array('adActive'=>0);
		$this->db->update('advertisements',$array);
		if($this->db->affected_rows()==1)
		{
			print json_encode(array("status"=>"success", "message"=>''));
		}
	}

	function aj_ad_display()
	{
		$post=$this->input->postArray();
		if($post['display']==1)
		{
			$post['display']=0;
		} else {
			$post['display']=1;
		}
		//$this->printCheck($post);
		$array = array('display'=>$post['display']);
		$this->db->where('adId',$post['adId']);
		$this->db->update('advertisements',$array);
		//$this->printCheck($this->db->last_query());
		if($this->db->affected_rows()==1)
		{
			print json_encode(array("status"=>"success", "message"=>$post['display']));
		}
	}

	function aj_imageDelete()
	{
		$post = $this->input->post();
		$this->db->where('galleryImageId',$post['galleryImageId']);
		$array = array('imageActive'=>2);
		$this->db->update('gallery_images',$array);
		if($this->db->affected_rows()==1)
		{
			print json_encode(array("status"=>"success", "message"=>''));
		}
	}

	function aj_galleryDelete()
	{
		$post = $this->input->post();
		$this->db->where('galleryId',$post['galleryId']);
		$array = array('galleryActive'=>2);
		$this->db->update('photo_galleries',$array);
		if($this->db->affected_rows()==1)
		{
			print json_encode(array("status"=>"success", "message"=>''));
		}
	}

	public function newsletter_list()
	{
		$data['mainContent'] = 'admin/newsletters_list';
		$data['sidebar']='sidebars/main_sidebar';
		$data['newsletters']=$this->newsletters->getDocuments(0,2);
		$data['title']='Harris Towers - Newsletters';
		$data['additionalHeadInfo']='';
		$this->load->view('templates/template',$data);
	}

	public function newsletter_edit()
	{
		$newsletterId = $this->uri->segment(2);
		if($newsletterId)
		{
			$data['newsletter']=$this->newsletters->getDocuments($newsletterId);
		}
		$post=$this->input->rawPostArray();
		if(isset($post['submit']))
		{
			$post['creationDateTime']=date('Y-m-d',time());
			$post['active']=1;
			$post['newsletterBody']=$this->input->post('newsletterBody',FALSE);
			// echo '<xmp>'; print_r($post);die;
			$this->newsletters->loadFromArray($post);
			$this->newsletters->save();
			$this->session->addMessage('Newsletter posted successfully!');
			redirect(base_url().'admin/newsletter_list');
		} else {
			$data['mainContent'] = 'admin/newsletter_edit';
			$data['sidebar']='sidebars/main_sidebar';
			$data['title']='Harris Towers - Newsletter';
			$data['additionalHeadInfo']='
			<script src="'.base_url().'assets/lib/ckeditor/ckeditor.js"></script>
			';
			$this->load->view('templates/template',$data);
		}
	}

	function aj_newsletterViewable()
	{
		$data=$this->input->getArray();
		if($data['active']==1)
		{
			$active=0;
		} else {
			$active = 1;
		}
		$this->db->where('newsletterId',$data['newsletterId']);
		$array = array('active'=>$active);
		$this->db->update('newsletters',$array);
		if($this->db->affected_rows() >0)
		{
			print json_encode(array("status"=>"success","message"=>$active));
		}
	}

	function aj_newsletterDelete()
	{
		$data=$this->input->getArray();
		$this->db->where('newsletterId',$data['newsletterId']);
		$array = array('active'=>2);
		$this->db->update('newsletters',$array);
		if($this->db->affected_rows() >0)
		{
			print json_encode(array("status"=>"success"));
		}
	}

	function aj_delete_event()
	{
		$data=$this->input->postArray();
		$this->db->where('eventId',$data['eventId']);
		$array = array('eventDisplay'=>0);
		$this->db->update('events',$array);
		if($this->db->affected_rows() >0)
		{
			print json_encode(array("status"=>"success"));
		}
	}
	function admin_events()
	{
		$post = $this->input->postArray();
		if($post)
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('eventTitle','Event Title','trim|required');
			$this->form_validation->set_rules('eventData','Event Body','trim|required');
			$this->form_validation->set_rules('eventDate','Date','trim|required');
			if($this->form_validation->run()==FALSE)
			{
				$data['mainContent'] = 'admin/admin_events';
				$data['title']='Harris Towers - Events';
				$data['eventId'] = $post['eventId'];
				$data['event'] = $this->events->getEvents($data['eventId']);
				$data['additionalHeadInfo']='
					<script type="text/javascript" src="'.base_url().'assets/lib/jquery-ui-1.8.22.custom.min.js"></script>
					<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
				';
				$this->load->view('templates/template-full',$data);
				return false;
			} else {
				$post['eventActive']=1;
				$post['eventDisplay']=1;
				$post['createdBy'] = $this->session->userdata('userId');
				$post['requestEmail'] = ' ';
				$this->events->loadFromArray($post);
				$this->events->save();
				$this->session->addMessage('Event saved!');
				redirect('admin_events');
			}
		}
		$this->load->model('events');
		$data['events']=$this->events->getEvents();
		$data['mainContent'] = 'admin/admin_events';
		$data['title']='Harris Towers - Events';
		$data['additionalHeadInfo']='
			<script type="text/javascript" src="'.base_url().'assets/lib/jquery-ui-1.8.22.custom.min.js"></script>
			<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
		';
		$this->load->view('templates/template-full',$data);
	}

	function edit_events()
	{
		$post = $this->input->postArray();
		if($post)
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('eventTitle','Event Title','trim|required');
			$this->form_validation->set_rules('eventData','Event Body','trim|required');
			$this->form_validation->set_rules('eventDate','Date','trim|required');
			if($this->form_validation->run()==FALSE)
			{
				$data['mainContent'] = 'admin/edit_events';
				$data['title']='Harris Towers - Events';
				$data['eventId'] = $post['eventId'];
				$data['event'] = $this->events->getEvents($data['eventId']);
				$data['additionalHeadInfo']='
					<script type="text/javascript" src="'.base_url().'assets/lib/jquery-ui-1.8.22.custom.min.js"></script>
					<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
				';
				$this->load->view('templates/template-full',$data);
				return false;
			} else {
				$post['eventDisplay']=1;
				$this->events->loadFromArray($post);
				$this->events->save();
				$this->session->addMessage('Event saved!');
				redirect('admin_events');
			}
		}
		if($post)
		{
			$data['eventId'] = $post['eventId'];
		}else{
			$data['eventId'] = $this->uri->segment(2);
		}
		$eventId = $this->uri->segment(2);
		$data['event'] = $this->events->getEvents($data['eventId']);
		$data['mainContent'] = 'admin/edit_events';
		$data['title']='Harris Towers - Events';
		$data['additionalHeadInfo']='
			<script type="text/javascript" src="'.base_url().'assets/lib/jquery-ui-1.8.22.custom.min.js"></script>
			<link rel="stylesheet" type="text/css" href="'.base_url().'assets/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
		';
		$this->load->view('templates/template-full',$data);

	}


	function aj_event_display()
	{
		$post=$this->input->postArray();
		if($post['eventActive']==1)
		{
			$post['eventActive']=0;
			$message = 'There was a problem with your party room request, please contact Doug for details at harristowers@shaw.ca.';
			$subject = 'Your party room request';
			$email = $post['requestEmail'];
			if($email)
			{
				$this->sendUserEmail($message,$subject,$email);
			}
		} else {
			$post['eventActive']=1;
			$subject = 'Your party room request';
			$message = 'Your party room request has been approved.';
			$email = $post['requestEmail'];
			if($email)
			{
				$this->sendUserEmail($message,$subject,$email);
			}
		}
		//$this->printCheck($post);
		$array = array('eventActive'=>$post['eventActive']);
		$this->db->where('eventId',$post['eventId']);
		$this->db->update('events',$array);
		//$this->printCheck($this->db->last_query());
		if($this->db->affected_rows()==1)
		{
			print json_encode(array("status"=>"success", "message"=>$post['eventActive']));
		}
	}


	public function periodic_list()
	{
		$data['mainContent'] = 'admin/periodic_list';
		$data['sidebar']='sidebars/main_sidebar';
		$data['newsletters']=$this->newsletters->getPeriodic(0,2);
		$data['title']='Harris Towers - Newsletters';
		$data['additionalHeadInfo']='';
		$this->load->view('templates/template',$data);
	}
}
