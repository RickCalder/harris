<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MY_Controller {

	public function test()
	{
		$this->printCheck($this->session->all_userdata());
	}


	public function index()
	{
		$data['mainContent'] = 'index';
		$data['sidebar']='sidebars/main_sidebar';
		$data['title']='Harris Towers - Home';
		$data['additionalHeadInfo'] = '<script type="text/javascript" charset="utf-8" src="'.base_url().'assets/lib/jquery.flexslider-min.js"></script>
			<script type="text/javascript">
				$(window).load(function() {
					$("#home-slider").flexslider({
						animation:"fade",
						easing: "swing"
					});
				});
			</script>';
		$this->load->view('templates/template',$data);
	}

	public function contact()
	{
		$data['mainContent'] = 'contact';
		$data['sidebar']='sidebars/main_sidebar';
		$data['title']='Contact Harris Towers';
		$data['additionalHeadInfo']='';
		$this->load->view('templates/template',$data);
	}

	public function public_classifieds()
	{
		$this->load->model('advertisements');
		$data['advertisements'] = $this->advertisements->getAdsForDisplay(1);
		$data['mainContent'] = 'classifieds';
		$data['title']='Harris Towers - Classified Ads';
		$data['additionalHeadInfo']='';
		$this->load->view('templates/template-full',$data);
	}

	public function contractor()
	{
		$data['mainContent'] = 'contractor';
		$data['sidebar']='sidebars/main_sidebar';
		$data['title']='Contractor Rules';
		$data['additionalHeadInfo']='';
		$this->load->view('templates/template',$data);
	}

	public function login() {
		$data=$this->input->postArray();
		$this->load->library('user_agent');
		if($this->session->login($data['userName'],$data['password']))
		{
			if($this->session->userdata('new_pass')==1)
			{
				$this->session->addMessage('We see you have reset your password, we suggest you <a href="drawPassword">click here</a> to change it to something you can easily remember.');
			}
			if ($this->agent->is_referral())
			{
				$referrer =$this->agent->referrer();
				redirect($referrer);
			} else {
				redirect('/');
			}
		} else {
		$this->session->addError('The account information you have entered is incorrect. Please contact the superintendent to retrieve your login credentials.');
			redirect('/');
		}
	}

	public function logout()
	{
		$this->session->logout();
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function list_users()
	{
		$this->db->join('people_users','people_users.userId = users.userId');
		$this->db->join('people','people_users.personId=people.personId');
		$this->db->join('buildings','users.buildingId=buildings.buildingId');
		$this->db->join('addresses','addresses.addressId=buildings.buildingAddressId');
		$data=$this->db->get('users');
		$this->printCheck($data->result_array());
	}
}
