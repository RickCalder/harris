<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "pages";
//$route['^(?!pages).*'] = "pages/$0";
$route['404_override'] = '';
$route['test']='pages/test';
$route['contact']='pages/contact';
$route['users/:num']='admin/users/$1';
$route['post_ad']='owners/owner_advertisements';
$route['approved_contractors']='owners/approved_contractors';
$route['new_user']='admin/new_user';
$route['admin_events']='admin/admin_events';
$route['edit_events/(:num)']='admin/edit_events/$1';
$route['admin_board']='admin/admin_board';
$route['admin_view_gallery/:num']='admin/admin_view_gallery';
$route['view_gallery/:num']='owners/view_gallery';
$route['galleries']='owners/galleries';
$route['announce']='owners/announce';
$route['classifieds']='owners/classifieds';
$route['book_party']='owners/book_party';
$route['public_classifieds']='pages/public_classifieds';
$route['users']='admin/users';
$route['logout']='pages/logout';
$route['contractor']='pages/contractor';
$route['login']='pages/login';
$route['newsletters/(:any)']='owners/newsletters/$1';
$route['periodics/(:any)']='owners/periodics/$1';
$route['newsletters']='owners/newsletters';
$route['periodics']='owners/periodics';
$route['events']='owners/events';
$route['owners_home']='owners/owners_landing';
$route['events(:any)']='owners/events/$1';
$route['owners_contact']='owners/owners_contact';
$route['feedback']='owners/feedback';
$route['newsletters_archive']='owners/newsletters_archive';
$route['periodics_archive']='owners/periodics_archive';
$route['newsletter_edit/(:any)']='admin/newsletter_edit/$1';
$route['newsletter_edit']='admin/newsletter_edit';
$route['admin_image_gallery']='admin/image_gallery';
$route['newsletter_list']='admin/newsletter_list';
$route['edit_announcement/:num']='admin/edit_announcement/$1';
$route['announcements']='admin/announcements';
$route['admin_advertisements']='admin/admin_advertisements';
$route['edit_advertisement/(:num)']='admin/edit_advertisement/$1';
$route['edit_ad/(:num)']='owners/edit_ad/$1';
$route['test']='admin/test';
$route['admin_contractors']='admin/admin_contractors';
$route['admin_pi']='admin/admin_pi';
$route['add_contractor_type']='admin/add_contractor_type';
$route['edit_contractor/(:num)']='admin/edit_contractor/$1';


/* End of file routes.php */
/* Location: ./application/config/routes.php */