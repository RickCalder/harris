<?PHP
class User_Controller extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		// If the user is using a mobile, use a mobile theme
		$this->load->library('user_agent');
		if (!$this->session->userdata('isLoggedIn')) {
			$this->session->addError("We're sorry, but you must be logged in to view that page.");
			redirect("/");
		}
	}
}
?>