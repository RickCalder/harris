<?PHP
class MY_Controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$user_id = $this->session->userdata('user_id');
		$this->load->model('announcements');
		$this->announcements = $this->announcements->displayAnnouncements();
		$this->load->model('advertisements');
		$this->ads = $this->advertisements->displayAds();

	}

	function printCheck($data)
	{
		echo '<pre>';
		print_r($data);
		echo '</pre>';
		die;
	}

	function sendEmail($message,$subject)
	{
		$config = array(
			'protocol'=>'smtp',
			'smtp_host'=>'mail.harristowers.com',
			'smtp_user'=>'noreply+harristowers.com',
			'smtp_pass'=>'ht_02-07-2013_cald3r',
			'mailtype'=>'html'
		);
		$this->load->library('email',$config);
		$this->email->from('noreply@harristowers.com','Harris Towers');
		$this->email->reply_to('noreply@harristowers.com','Harris Towers');
		$this->email->to('calder408@gmail.com');
		$this->email->cc('rick@calderonline.com');
		$this->email->cc('harristowers@rogers.com');
		$this->email->subject($subject);
		$this->email->message($message);
		if($this->email->send())
		{
			$this->session->addMessage('Your message was sent to the administrator.');
			return true;
		} else {
			$this->session->addError('Sorry, something went wrong, please try again later.');
			return true;
		}
	}
	function sendUserEmail($message,$subject,$email)
	{
		$config = array(
			'protocol'=>'smtp',
			'smtp_host'=>'mail.harristowers.com',
			'smtp_user'=>'noreply+harristowers.com',
			'smtp_pass'=>'ht_02-07-2013_cald3r',
			'mailtype'=>'html'
		);
		$this->load->library('email',$config);
		$this->email->from('noreply@harristowers.com','Harris Towers');
		$this->email->reply_to('noreply@harristowers.com','Harris Towers');
		$this->email->to($email);
		$this->email->subject($subject);
		$this->email->message($message);
		if($this->email->send())
		{
			$this->session->addMessage('Your message was sent to the user with the address '.$email);
			return true;
		} else {
			$this->session->addError('Sorry, something went wrong, please try again later or you can notify the user manually at '.$email);
			return true;
		}
	}

	function imageUpload($control, $path, $imageName,$sizes='all')
	{
		if( ! isset($control) || ! is_uploaded_file($control['tmp_name']))
		{
			$images =('No file was chosen MY');
			return FALSE;
		}
		if($control['size']>4096000)
		{
			$images =('File is too large ('.round(($control["size"]/1000)).'kb), please choose a file under 2,048kb');
			return FALSE;
		}
		if($control['error'] !== UPLOAD_ERR_OK)
		{
			$images =('Upload failed. Error code: '.$control['error']);
			Return FALSE;
		}
		switch(strtolower($control['type']))
		{
			case 'image/jpeg':
			$image = imagecreatefromjpeg($control['tmp_name']);
			move_uploaded_file($control["tmp_name"],$path.$imageName);
			break;
			case 'image/png':
			$image = imagecreatefrompng($control['tmp_name']);
			move_uploaded_file($control["tmp_name"],$path.$imageName);
			break;
			case 'image/gif':
			$image = imagecreatefromgif($control['tmp_name']);
			move_uploaded_file($control["tmp_name"],$path.$imageName);
			break;
			default:
			print('This file type is not allowed');
			return false;
		}
		@unlink($control['tmp_name']);
		$old_width      = imagesx($image);
		$old_height     = imagesy($image);

		//Create med version
		if ($sizes=='med' || $sizes=='all')
		{
			$max_width = 150;
			$max_height = 150;
			$scale          = min($max_width/$old_width, $max_height/$old_height);
			if ($old_width > 150 || $old_height > 150)
			{
				$new_width      = ceil($scale*$old_width);
				$new_height     = ceil($scale*$old_height);
			} else {
				$new_width = $old_width;
				$new_height = $old_height;
			}
			$new = imagecreatetruecolor($new_width, $new_height);
			imagecopyresampled($new, $image,
			0, 0, 0, 0,
			$new_width, $new_height, $old_width, $old_height);
			switch(strtolower($control['type']))
			{
				case 'image/jpeg':
				imagejpeg($new, $path.'med_'.$imageName, 70);
				break;
				case 'image/png':
				imagealphablending($new, false);
				imagecopyresampled($new, $image,0, 0, 0, 0,$new_width, $new_height, $old_width, $old_height);
				imagesavealpha($new, true);
				imagepng($new, $path.'med_'.$imageName, 0);
				break;
				case 'image/gif':
				imagegif($new, $path.'med_'.$imageName);
				break;
				default:
			}
		}
		//Create med version
		if ($sizes=='lrg' || $sizes=='all')
		{
			$max_width = 1000;
			$max_height = 1000;
			$scale          = min($max_width/$old_width, $max_height/$old_height);
			if ($old_width > 1000 || $old_height > 1000)
			{
				$new_width      = ceil($scale*$old_width);
				$new_height     = ceil($scale*$old_height);
			} else {
				$new_width = $old_width;
				$new_height = $old_height;
			}
			$new = imagecreatetruecolor($new_width, $new_height);
			imagecopyresampled($new, $image,
			0, 0, 0, 0,
			$new_width, $new_height, $old_width, $old_height);
			switch(strtolower($control['type']))
			{
				case 'image/jpeg':
				imagejpeg($new, $path.'lrg_'.$imageName, 70);
				break;
				case 'image/png':
				imagealphablending($new, false);
				imagecopyresampled($new, $image,0, 0, 0, 0,$new_width, $new_height, $old_width, $old_height);
				imagesavealpha($new, true);
				imagepng($new, $path.'lrg_'.$imageName, 0);
				break;
				case 'image/gif':
				imagegif($new, $path.'lrg_'.$imageName);
				break;
				default:
			}
		}
		imagedestroy($image);
		imagedestroy($new);
		$images =  '<div style="font-family:arial;"><b>'.$imageName.'</b> Uploaded successfully. Size: '.round($control['size']/1000).'kb</div>';
		return $images;
	}
}