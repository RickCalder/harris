<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Model extends CI_Model {

	public $table;
	protected $primaryKey;

	/**
	* Constructor
	*
	* @access public
	*/
	public function __construct()
	{
		parent::__construct();
		if ($this->table == null) {
			$this->table = get_Class($this);
		}
		$this->load->database();
	}

	public function loadById($primaryKeyIdentifier) {
		if ($primaryKeyIdentifier != null) {
			$select = "
			SELECT *
			FROM " . $this->table . "
			WHERE " . $this->primaryKey . " = " . $primaryKeyIdentifier;

			$result = $this->db->query($select);

			if ($result->num_rows() == 1) {
				$data = $result->result_array();

				//die(print_r($data, true));

				$this->loadFromArray($data[0]);
			}
		}
	}
	public static function GetColumns(&$db, $tableName, $keyColumn, $valueColumn, $filter = "", $orderBy = null) {
		$select =
		"SELECT $keyColumn as myKey, $valueColumn as myVal
		FROM $tableName
		$filter
		ORDER BY " . ($orderBy == null ? $valueColumn : $orderBy);		
	
		$rslt = $db->query($select);
	
		$columns = array();
	
		foreach ($rslt->result_array() as $row) {
			$columns[$row['myKey']] = $row['myVal'];
		}
		
		return $columns;
	}

	/**
	* Takes an associative array, and scans it for keys that match
	* internal variable names.  On matches, set's the internal variable
	* to match the data in the array.
	*/
	public function loadFromArray($array)
	{
		$class = get_object_vars($this);
		if (!is_array($array)) {
			die(("Not an array"));
		}
		foreach($array as $key=>$value) {
			if (array_key_exists($key, $class)) {
				if (!is_array($value)) {
					$this->$key = trim($value);
					// Check to see if this is the primary key, if so, set the unique identifier
					if ($key == $this->primaryKey && $value != null) {
						$this->uniqueIdentifier = $key . " = " . $value;
					}
				}
				else {
					$this->$key = $value;
				}
			}
		}
	}

	public function save()
	{
		$data = get_object_vars($this);

		$dataToSave = array();

		$op = 'update';
		$keyExists = FALSE;
		$fields = $this->db->field_data($this->table);
		foreach ($fields as $field)
		{
			$dataToSave[$field->name] = $data[$field->name];

			if($field->primary_key==1)
			{
				if(isset($data[$field->name]) && $data[$field->name] != null)
				{
					$keyExists = TRUE;
					$this->db->where($field->name, $data[$field->name]);
				}
				else
				{
					$op = 'insert';
				}
			}
		}


		if($keyExists && $op=='update')
		{

			// We're doing an update, see if the data had an updatedDateTime and set it to NOW() if so.
			if (array_key_exists("updatedDateTime", $dataToSave)) {
				//$dataToSave['updatedDateTime'] = "NOW()";
				$this->db->set('updatedDateTime', "NOW()", false);
				unset($dataToSave['creationDateTime']);
				unset($dataToSave['updatedDateTime']);
			}
			$this->db->set($dataToSave);
			$this->db->update($this->table);
			return true;

			/*
			Based on how we use this, don't know why you'd want to do this part.
			if($this->db->affected_rows()==1)
			{
			return $this->db->affected_rows();
			}
			*/
		}


		$this->db->set($dataToSave);
		// We're doing an insert, see if the data had an creationDateTime and set it to NOW() if so.
		if (array_key_exists("creationDateTime", $dataToSave)) {
			//$dataToSave['creationDateTime'] = "NOW()";
			$this->db->set('creationDateTime', "NOW()", false);
		}
		$this->db->insert($this->table);

		$primaryKey = $this->primaryKey;

		$this->{$primaryKey} = mysqli_insert_id();

		return $this->db->affected_rows();
	}

	public function get($varName)
	{
		return $this->{$varName};
	}
	/*too lazy to type this out all the time, can be removed later*/
	function printCheck($data)
	{
		echo '<pre>';
		print_r($data);
		echo '</pre>';
		die;
	}

}