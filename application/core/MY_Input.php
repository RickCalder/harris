<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Input extends CI_Input {
	public function postArray() {
		$post = array();
		
		foreach ($_POST as $key=>$value) {
			$post[$key] = $this->security->xss_clean($value);
		}
		
		return $post;
	}
	public function rawPostArray() {
		$post = array();
		
		foreach ($_POST as $key=>$value) {
			$post[$key] = $value;
		}
		
		return $post;
	}
	public function getArray() {
		$get = array();
		
		foreach ($_GET as $key=>$value) {
			$get[$key] = $this->security->xss_clean($value);
		}
		
		return $get;		
	}
	
	public function requestArray() {
		$request = array();
		
		$request = $this->getArray() + $this->postArray();
		
		return $request;
	}
}