<?PHP
class Admin_Controller extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        
        if(!is_array($this->session->userdata('roleNames')) || !in_array(PRIV_ADMIN_BASIC, $this->session->userdata('roleNames')))
        {
            $this->session->addError('Sorry, that area is reserved for administrators.');
			redirect('/');
        }
        //else if (!in_array(strtolower(get_class($this)), $this->session->userdata('roleNames'))) {
        else if (false) {
        	$this->session->addError("Sorry, but you don't have access to this area of the website: " . get_class($this));
			redirect('/');
        }
    }
}
?>