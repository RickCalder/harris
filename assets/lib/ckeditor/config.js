/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http:/http://www.harristowers.com/assets/lib/ckeditor.com/license
 */
CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.contentsCss = 'http://www.harristowers.comhttp://www.harristowers.com/assets/css/main.css';
	config.filebrowserBrowseUrl = 'http://www.harristowers.com/assets/lib/ckeditor/kcfinder/browse.php?type=files';
	config.filebrowserImageBrowseUrl = 'http://www.harristowers.com/assets/lib/ckeditor/kcfinder/browse.php?type=images';
	config.filebrowserFlashBrowseUrl = 'http://www.harristowers.com/assets/lib/ckeditor/kcfinder/browse.php?type=flash';
	config.filebrowserUploadUrl = 'http://www.harristowers.com/assets/lib/ckeditor/kcfinder/upload.php?type=files';
	config.filebrowserImageUploadUrl = 'http://www.harristowers.com/assets/lib/ckeditor/kcfinder/upload.php?type=images';
	config.filebrowserFlashUploadUrl = 'http://www.harristowers.com/assets/lib/ckeditor/kcfinder/upload.php?type=flash';
};
